from django import forms

from django.contrib.auth import get_user_model
from django.forms import ModelForm

from shop.models import SHIPPING_TYPES

User = get_user_model()



class UserUpdateDeliveryBuyForm(ModelForm):
    shipping_type = forms.ChoiceField(choices=SHIPPING_TYPES)
    snippet = forms.CharField(max_length=256, label="Примітки для продавця", required=False)

    class Meta:
        model = User
        # Custom fields
        fields = ['shipping_type','pib', 'city', 'address', 'email', 'phone', 'snippet']