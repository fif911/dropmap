from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.http import HttpResponseForbidden
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.views.generic import CreateView, UpdateView
from django_filters.views import FilterView
from shop.filters import ItemFilter
from shop.forms import UserUpdateDeliveryBuyForm
from shop.models import Item, Order
from django.core.mail import send_mail
from django.contrib.auth import get_user_model

User = get_user_model()


class OrderClientRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        # if self.object.author != self.request.user:
        if Order.objects.get(pk=kwargs['pk']).client != self.request.user:
            return HttpResponseForbidden()
        return super(OrderClientRequiredMixin, self).dispatch(request, *args, **kwargs)


class AuthorRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        # if self.object.author != self.request.user:
        if Item.objects.get(pk=kwargs['pk']).author != self.request.user:
            return HttpResponseForbidden()
        return super(AuthorRequiredMixin, self).dispatch(request, *args, **kwargs)


def send_email(request):
    # subject = "anything"
    # html_content = template.format(arguments)
    # from_email, to = from_email, user_email
    # text_content = ''
    # msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    # msg.attach_alternative(html_content, "text/html")
    # msg.send()

    # send_mail('Катя привет', 'Скоро ты сможешь продавать вещи', 'noreply@dropmap.com.ua', ['sui.kate2001@gmail.com'])
    return render(request, 'shop/detail_item_old.html')


class OrderDetail(LoginRequiredMixin, OrderClientRequiredMixin, generic.DetailView):
    model = Order
    template_name = 'shop/order_detail.html'


class UpdateDelInfoBuy(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    ''' View for creating an order. Every field is required '''
    # todo check if item isn't bought before displaying view & creating order

    model = User
    form_class = UserUpdateDeliveryBuyForm
    login_url = '/login/'
    template_name = "shop/item_update.html"
    # set to just created order.
    success_url = '#'
    success_message = 'Інформація збережена'

    # fields = ['pib', 'city', 'address', 'email', 'phone']

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        response = super(UpdateDelInfoBuy, self).form_valid(form)
        # CREATE ORDER OBJ
        shipping_type =form.cleaned_data['shipping_type']
        total_cost = Item.objects.get(id = self.kwargs['pk']).price + int(shipping_type)
        order = Order.objects.create(item_id=self.kwargs['pk'], client_id=self.request.user.id, payment_status="TO_PAY",
                                     shipping_type=shipping_type,
                                     snippet=form.cleaned_data['snippet'], total_cost=total_cost)
        order.save()
        subject = "Замовлення з Dropmap #" + str(order.id)
        text = "Чек твого замовлення - річ -" + str(order.item.name)
        send_mail(subject, text, 'noreply@dropmap.com.ua',
                  ['sandrrichfif911@gmail.com'])
        # success_url = order.get_absolute_url()
        return HttpResponseRedirect(reverse('shop:order', kwargs={'pk': order.id}))


class ListView(FilterView):
    model = Item
    template_name = 'shop/list_item.html'
    # context_object_name = 'item_list'
    # context_object_name = 'filter.qs'
    paginate_by = 18
    # paginate_by = 1
    queryset = Item.objects.all().filter(status='AVAILABLE').filter(on_site=True).order_by('-promoting', '-created')
    filterset_class = ItemFilter

    # def get_context_data(self, **kwargs):
    #     contex = super().get_context_data(**kwargs)
    #     contex['filter'] = ItemFilter(self.request.GET,queryset=self.get_queryset())
    #     return contex


class ItemUpdateView(LoginRequiredMixin, AuthorRequiredMixin, UpdateView):
    model = Item
    login_url = '/login/'
    template_name = "shop/item_update.html"

    fields = ['name', 'price', 'condition', 'size', 'gender', 'description', 'condition_notes',
              'measurements']


class ItemCreateView(LoginRequiredMixin, CreateView):
    template_name = "shop/item_suggest.html"
    model = Item
    login_url = '/login/'
    # redirect_field_name = 'redirect_to'
    fields = ['name', 'price', 'condition', 'size', 'gender', 'description', 'condition_notes',
              'measurements', 'main_image', "image2", "image3", "image4", "image5"]

    def get_success_url(self):
        return reverse('shop:detail', args=(self.object.id,))

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.was_suggested = True
        instance.author = self.request.user
        instance.status = "AVAILABLE" if self.request.user.is_author_proved else "TO_CHECK"

        return super(ItemCreateView, self).form_valid(form)
        # instance.save()
        # return http.HttpResponseRedirect(self.get_success_url())


class ItemDetailView(generic.DetailView):
    model = Item
    template_name = 'shop/detail_item.html'
