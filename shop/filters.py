# import django_filters
from django.forms import CheckboxSelectMultiple
from django_filters import FilterSet, ChoiceFilter, MultipleChoiceFilter, BooleanFilter
from django_filters.widgets import BooleanWidget

from shop.models import Item

GENDER_TYPES = (
    ("U", "Унісекс"),
    ("M", "Чоловіча"),
    ("F", "Жіноча"),
)

AUTHOR_PROVED = (
    (True, "Тільки підтверджені"),
)


class ItemFilter(FilterSet):
    # gender = ChoiceFilter(choices=GENDER_TYPES, empty_label=None,
    #                                              widget=CheckboxSelectMultiple, method='filter_by_gender')

    # gender = django_filters.ChoiceFilter(choices=GENDER_TYPES, empty_label="Всі")
    author_proved = ChoiceFilter(choices=AUTHOR_PROVED, empty_label="Усі",
                                 field_name='author__is_author_proved')
    gender = MultipleChoiceFilter(choices=GENDER_TYPES, widget=CheckboxSelectMultiple)


    class Meta:
        model = Item
        fields = ['size', 'condition']