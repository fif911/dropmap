from django.contrib import admin
from shop.models import Item, Order


# Register your models here.
class ItemAdmin(admin.ModelAdmin):
    # fieldsets = [
    #     (None,               {'fields': ['question_text']}),
    #     ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    # ]
    list_display = ('name', 'status', 'was_suggested', 'promoting')
    list_filter = ['status', 'was_suggested', 'author__is_author_proved', 'gender', 'condition', 'on_site', 'promoting']
    search_fields = ['name', 'author__username']
    # readonly_fields = ('owner',)

    # def save_model(self, request, obj, form, change):
    #     obj.owner = request.user
    #     super().save_model(request, obj, form, change)

    # Display only objects which user has created
    # def queryset(self, request):
    #     request_user = request.user
    #     return Post.objects.filter(author=request_user)


class OrderAdmin(admin.ModelAdmin):
    list_display = ('item', 'id', 'payment_status', 'client', 'viewed')
    list_filter = ['payment_status', 'viewed']


admin.site.register(Order, OrderAdmin)

admin.site.register(Item, ItemAdmin)
