from django.core.validators import RegexValidator
from django.db import models
from django.urls import reverse
from django.conf import settings
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill
from django.utils.text import slugify


def upload_location(instance, filename):
    return "shop/%s/%s" % (slugify(instance.name, allow_unicode=True), filename)


ITEM_CONDITION_TYPES = (
    ("SC", "Секонд, є нюанси"),
    ("ST", "Секонд але як нова"),
    ("BN", "Нова річ"),
)

GENDER_TYPES = (
    ("U", "Унісекс"),
    ("M", "Чоловіча"),
    ("F", "Жіноча"),
)

ITEM_SIZES = (
    ("ONE_SIZE", "One size"),
    ("XXS", "XXS"),
    ("XS", "XS"),
    ("S", "S"),
    ("M", "M"),
    ("L", "L"),
    ("XL", "XL"),
    ("XXL", "XXL"),
)

SIZES_CHOICES = [
    ('Одяг', (
        ("XXS", "XXS"),
        ("XS", "XS"),
        ("S", "S"),
        ("M", "M"),
        ("L", "L"),
        ("XL", "XL"),
        ("XXL", "XXL"),
        )
    ),
    ('Взуття', (
            ('41', '41'),
            ('42', '42'),
        )
    ),
    ('ONE_SIZE', 'One size'),
]

STATUSES = (
    ("AVAILABLE", "Продається"),
    ("TO_CHECK", "Чекає підтвердження"),
    ("SOLD", "Продана"),
    # ("BASKET", "В кошику"),
    # ("PAUSED", "Призупинено"),
)

User = settings.AUTH_USER_MODEL


# Create your models here.
class Item(models.Model):
    # todo add category, size + smart filters to it
    name = models.CharField(max_length=50)
    price = models.PositiveSmallIntegerField()
    condition = models.CharField(max_length=4, choices=ITEM_CONDITION_TYPES)
    size = models.CharField(max_length=10, choices=SIZES_CHOICES)
    gender = models.CharField(max_length=3, choices=GENDER_TYPES, default=GENDER_TYPES[0])

    author = models.ForeignKey(User, on_delete=models.CASCADE)

    description = models.CharField(max_length=256, blank=True, null=True)
    condition_notes = models.CharField(max_length=256, blank=True, null=True)
    measurements = models.CharField(max_length=256, blank=True, null=True)
    status = models.CharField(max_length=9, choices=STATUSES, default=STATUSES[0])

    was_suggested = models.BooleanField(default=False,
                                        help_text="показує чи взагалі цю річ додали через адмінку або через форму")
    promoting = models.BooleanField(default=False, help_text="Просування речі. Викидує її на перші сторінки")
    on_site = models.BooleanField(default=True,
                                  help_text='Глобальна настройка. Просто забирає річ з списків якщо вимкнена')
    created = models.DateTimeField(auto_now_add=True)

    # todo main {list square 170x170 , slider 390x390 , full quality bit lower}. others {slider , full}
    # main image original & cropping


    main_image = ProcessedImageField(upload_to=upload_location,
                                     format='JPEG',
                                     options={'quality': 80}, verbose_name="Головне фото")

    # slider_image = ImageSpecField(source='main_image',
    #                               processors=[ResizeToFill(390, 390)],
    #                               format='JPEG',
    #                               options={'quality': 80})
    # additional images
    image2 = ProcessedImageField(upload_to=upload_location,
                                 format='JPEG',
                                 options={'quality': 80}, blank=True, null=True)
    # slider_image2 = ProcessedImageField(upload_to=upload_location,
    #                                     processors=[ResizeToFill(390, 390)],
    #                                     format='JPEG',
    #                                     options={'quality': 80}, blank=True, null=True)
    image3 = ProcessedImageField(upload_to=upload_location,
                                 format='JPEG',
                                 options={'quality': 80}, blank=True, null=True)
    # slider_image3 = ProcessedImageField(upload_to=upload_location,
    #                                     processors=[ResizeToFill(390, 390)],
    #                                     format='JPEG',
    #                                     options={'quality': 80}, blank=True, null=True)
    image4 = ProcessedImageField(upload_to=upload_location,
                                 format='JPEG',
                                 options={'quality': 80}, blank=True, null=True)
    # slider_image4 = ProcessedImageField(upload_to=upload_location,
    #                                     processors=[ResizeToFill(390, 390)],
    #                                     format='JPEG',
    #                                     options={'quality': 80}, blank=True, null=True)
    image5 = ProcessedImageField(upload_to=upload_location,
                                 format='JPEG',
                                 options={'quality': 80}, blank=True, null=True)

    # slider_image5 = ProcessedImageField(upload_to=upload_location,
    #                                     processors=[ResizeToFill(390, 390)],
    #                                     format='JPEG',
    #                                     options={'quality': 80}, blank=True, null=True)

    def image2_exist(self):
        return True if self.image2 else None

    def image3_exist(self):
        return True if self.image2 else None

    def image4_exist(self):
        return True if self.image2 else None

    def image5_exist(self):
        return True if self.image2 else None

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:detail', kwargs={'pk': self.id})

    # def save(self, *args, **kwargs):
    #     super(Item, self).save(*args, **kwargs)


PAYMENT_STATUSES = (
    ("TO_PAY", "Чекає оплати"),
    ("NOT_ENOUGH", "Не достатньо оплачено"),
    ("PAID", "Оплачено"),
)
SHIPPING_TYPES = (
    (0, "Самовивіз + 0 грн"),
    (20, "УКР Поштою по місту/області + 20 грн"),
    (27, "УКР Поштою по Україні + 27 грн"),
    (45, "Новою поштому по місту + 45 грн"),
    (50, "Новою поштому по місту + 50 грн"),
    (55, "Новою Поштою по Україні + 55 грн"),
)


class Order(models.Model):
    item = models.ForeignKey(Item, null=True, on_delete=models.SET_NULL)
    payment_status = models.CharField(max_length=9, default=PAYMENT_STATUSES[0], choices=PAYMENT_STATUSES)
    client = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    shipping_type = models.IntegerField(choices=SHIPPING_TYPES)
    total_cost = models.IntegerField()
    snippet = models.TextField(max_length=256, verbose_name="Примітки для продавця", blank=True, null=True)
    viewed = models.BooleanField(default=False)
    # info which is filling on pay check
    paid_by = models.CharField(max_length=100, blank=True, null=True)
    pay_comment = models.CharField(max_length=256, blank=True, null=True)
    pay_amount = models.CharField(max_length=256, blank=True, null=True)
    not_enough = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse('shop:order', kwargs={'pk': self.id})

    def __str__(self):
        return self.item.name if self.item.name else 'Item was deleted'
