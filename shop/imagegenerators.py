from imagekit import ImageSpec, register
from imagekit.processors import ResizeToFill

class Thumbnail_List(ImageSpec):
    processors = [ResizeToFill(170, 170)]
    format = 'JPEG'
    options = {'quality': 80}

class Thumbnail_Slider(ImageSpec):
    processors = [ResizeToFill(390, 390)]
    format = 'JPEG'
    options = {'quality': 80}

register.generator('shop:thumbnail_list', Thumbnail_Slider)
register.generator('shop:thumbnail_slider', Thumbnail_Slider)