from django.urls import path

from . import views

app_name = 'shop'

urlpatterns = [
    # path('shop/', views.items_list, name='list'),
    path('shop/', views.ListView.as_view(), name='list'),
    path('shop/test', views.send_email, name='email'),
    path('shop/create-item/', views.ItemCreateView.as_view(), name='create'),
    path('shop/<int:pk>/edit', views.ItemUpdateView.as_view(), name='update'),
    path('shop/<int:pk>/', views.ItemDetailView.as_view(), name='detail'),
    path('shop/<int:pk>/buy', views.UpdateDelInfoBuy.as_view(), name='buy'),
    path('shop/order/<int:pk>', views.OrderDetail.as_view(), name='order'),
]
