from django.urls import path
from django.views.generic import TemplateView

from .views import (GetUserFav, UpdateUser, ListUserOrders)

app_name = 'shop-api'
urlpatterns = [
    path('<pk>/add_to_favourite', UpdateUser.as_view(), name='add_to_favourite'),
    path('get_user_favourites', GetUserFav.as_view(), name='get_user_favourites'),
    path('update_user_payment_status', ListUserOrders.as_view(), name='update_user_payment_status'),

]
