import re
from datetime import datetime, timedelta
from django.contrib.auth import get_user_model
from django.http import Http404
from rest_framework import permissions, status
from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from shop.api.serializers import UserSerializer, OrderSerializer
from shop.models import Item, Order
from hashlib import sha1, md5
from requests import post

User = get_user_model()


class GetUserFav(APIView):

    def get(self, request, format=None):
        user = User.objects.get(username=self.request.user)
        serializer = UserSerializer(user)
        return Response(serializer.data)


class UpdateUser(APIView):
    def put(self, request, pk, format=None):
        user = User.objects.get(username=self.request.user)
        # snippet = self.get_object(pk)
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListUserOrders(APIView):
    """
    View to update statuses of orders and return results
    """

    # authentication_classes = [authentication.TokenAuthentication]
    # permission_classes = [permissions.IsAdminUser]

    def get(self, request, format=None):
        queryset = Order.objects.all()
        # todo fix logic in future. it's silly to return all the
        # todo get last payments here and update values

        url = "https://api.privatbank.ua/p24api/rest_fiz"
        password = "L8Lp1g9w35ycGWAktvy18IE4WOvQ33Uu"
        head = """<?xml version="1.0" encoding="UTF-8"?>
        <request version="1.0">
            <merchant>
                <id>148717</id>
                <signature>"""
        end_head = """</signature>
            </merchant>
            <data>
                """
        data = """<oper>cmt</oper>
                            <wait>0</wait>
                            <test>0</test>
                            <payment id="">
                                <prop name="sd" value="{:%d.%m.%Y}" />
                                <prop name="ed" value="{:%d.%m.%Y}" />
                                <prop name="card" value="5168755436867846" />
                            </payment>""".format(datetime.today() - timedelta(days=2), datetime.today())

        footer = """
            </data>
        </request>"""

        signature_md5 = md5((data + password).encode('utf-8')).hexdigest()
        signature_done = sha1(signature_md5.encode('utf-8')).hexdigest()

        data_done = head + signature_done + end_head + data + footer

        res = post(url, data=data_done, headers={'Content-Type': 'application/xml; charset=UTF-8'})
        # https://regex101.com/r/6Ga089/3 url for reg exp

        # just for testing
        # text = '<?xml version="1.0" encoding="UTF-8"?><response version="1.0"><merchant><id>148717</id><signature>ada4d659f205ab66600d586039da1415ed1e05bb</signature></merchant><data><oper>cmt</oper><info><statements status="excellent" credit="401.0" debet="872.52"><statement card="5168755436867846" appcode="301017" trandate="2019-09-05" trantime="22:39:00" amount="1.00 UAH" cardamount="1.00 UAH" rest="346.61 UAH" terminal="PrivatBank, CS980400" description="Перевод с карты ПриватБанка через приложение Приват24. Отправитель: Сухарева Катерина Костянтинівна. Комментарий к платежу: заказ 123 курлык."/><statement card="5168755436867846" appcode="737025" trandate="2019-08-30" trantime="13:59:00" amount="104.50 UAH" cardamount="-110.00 UAH" rest="345.61 UAH" terminal="Vintage, L1IF2MPT" description="Одежда: Second Hand Vintage, Iвано-Франкiвськ, Коновальца, 100"/><statement card="5168755436867846" appcode="366016" trandate="2019-08-20" trantime="16:14:00" amount="200.00 UAH" cardamount="200.00 UAH" rest="455.61 UAH" terminal="PrivatBank, CS980400" description="Перевод с карты ПриватБанка через приложение Приват24. Отправитель: Паршуков Іван Олександрович"/><statement card="5168755436867846" appcode="255015" trandate="2019-08-20" trantime="15:58:00" amount="200.00 UAH" cardamount="200.00 UAH" rest="255.61 UAH" terminal="PrivatBank, CS980400" description="Перевод с карты ПриватБанка через Приват24. Отправитель: Товт Василь Васильович"/><statement card="5168755436867846" appcode="96202Z" trandate="2019-08-18" trantime="12:41:00" amount="257.52 UAH" cardamount="-257.52 UAH" rest="55.61 UAH" terminal="GIPERMARKET EPICENTR, 20340222" description="Ремонт/Строительство: GIPERMARKET EPICENTR, Ivano-Frankiv"/><statement card="5168755436867846" appcode="532023" trandate="2019-08-11" trantime="17:36:00" amount="505.00 UAH" cardamount="-505.00 UAH" rest="313.13 UAH" terminal="P24 DB M UA, I0110SWA" description="Перевод со своей карты"/></statements></info></data></response>'

        text = '\n<statement card="5168755436867846" appcode="301017" trandate="2019-09-05" trantime="22:39:00" amount="999999999.00 UAH" cardamount="1.00 UAH" rest="346.61 UAH" terminal="PrivatBank, CS980400" description="Перевод с карты ПриватБанка через приложение Приват24. Отправитель: alex zako. Комментарий к платежу: заказ 13."/>'
        # todo change to res.text in matches
        matches = re.findall(
            '^<statement.+?amount="([0-9]+\.[0-9]+) UAH".+?Отправитель: (.+?). Комментарий к платежу: (.*?(\d+).*?)."\/>$',
            text, re.M)

        if matches:
            for match in matches:

                # o = queryset.filter(item__price__gte=float(match[0])).filter(id=int(match[3])).first()
                try:
                    # the logic is someone else may pay order
                    o = Order.objects.get(payment_status="TO_PAY", id=int(match[3]))
                except Order.DoesNotExist:
                    o = None

                if o:
                    if float(o.total_cost) > float(match[0]):
                        o.payment_status = "NOT_ENOUGH"
                        # todo notify me if someone pays not enough
                    else:
                        o.payment_status = "PAID"

                    o.paid_by = match[1]
                    o.pay_comment = match[2]
                    o.pay_amount = float(match[0])
                    o.save()
                    # todo sent email to author here. That item was sold

        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)
