from django.contrib.auth import get_user_model
from rest_framework import serializers

from shop.models import Item, Order

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    def update(self, instance, validated_data):
        item = validated_data.get('liked_items', instance.liked_items)[0]
        get_all_related = instance.liked_items.all()
        all_related = get_all_related | Item.objects.filter(pk=item.pk)
        instance.liked_items.set(all_related)
        # instance.content = validated_data.get('content', instance.content)
        # instance.created = validated_data.get('created', instance.created)
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ['liked_items']


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = [
            'id',
            'payment_status',
            # 'geometry',
        ]
