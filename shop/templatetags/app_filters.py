from django import template
from datetime import date, timedelta

register = template.Library()
import re


@register.filter(name='fix_url')
def fix_url(url):
    if "/?" in url:
        # here was some req
        # todo search here for "path=[0-9]" and delete it
        # shop/?size=XXS&type=&page=2&page=1
        # make shop/?size=XXS&type=&page=1
        # re.sub('<.*?>', '', string)
        # print(url)
        # if (re.search(r"path=", url)):
        #     print("MATCH")
        url = re.sub('(page=\d+)', '', url)
        # print(url)
        if re.search('/\?.+', url):
            url += "&"
            url = re.sub('&+', '&', url)

    else:
        url += "?"

    return url
