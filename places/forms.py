from django import forms
from .models import Second

class SecondForm(forms.ModelForm):
    class Meta:
        model = Second
        fields =('name', )