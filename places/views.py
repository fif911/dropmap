from django.core.exceptions import ObjectDoesNotExist
from django.views import generic
from django.views.generic import CreateView
from django.core.paginator import Paginator

from places.models import Second, SuggetionPlace, Stock, Schedule


class MapSecondsView(generic.TemplateView):
    template_name = 'places/map_seconds.html'

class MapStockView(generic.TemplateView):
    template_name = 'places/map_stocks.html'

# todo separate templates for detail view ? design ?
class SecondDetailView(generic.DetailView):
    model = Second
    context_object_name = 'place'
    template_name = 'places/detail_place.html'

    def get_context_data(self, **kwargs):
        context = super(SecondDetailView, self).get_context_data(**kwargs)
        context['type'] = "секонд"
        context['additional_info'] = self.object.phone or self.object.phone
        context['second_list'] = Second.objects.order_by('-rating', '?')[:4]
        try:
            context['shedule'] = Schedule.objects.get(city = self.object.id)
        except ObjectDoesNotExist:
            pass
        return context


class StockDetailView(generic.DetailView):
    model = Stock
    context_object_name = 'place'
    template_name = 'places/detail_place.html'

    def get_context_data(self, **kwargs):
        context = super(StockDetailView, self).get_context_data(**kwargs)
        context['type'] = "сток"
        context['second_list'] = Stock.objects.order_by('-rating', '?')[:4]

        return context


class SecondListView(generic.ListView):
    # model = Second
    template_name = 'places/place_list.html'
    context_object_name = 'second_list'
    paginate_by = 12
    # paginate_by = 2
    queryset = Second.objects.all().filter(show_on_map=True).order_by('-rating')  # Default: Model.objects.all()

    def get_context_data(self, **kwargs):
        context = super(SecondListView, self).get_context_data(**kwargs)
        context['type'] = "second"
        return context

class StockListView(generic.ListView):
    template_name = 'places/place_list.html'
    context_object_name = 'second_list'
    paginate_by = 12
    # paginate_by = 2
    queryset = Stock.objects.all().filter(show_on_map=True).order_by('-rating')  # Default: Model.objects.all()

    def get_context_data(self, **kwargs):
        context = super(StockListView, self).get_context_data(**kwargs)
        context['type'] = "stock"
        return context


class SuggestPlacetView(CreateView):
    template_name = "places/place_suggest.html"
    model = SuggetionPlace
    success_url = "/"
    fields = ['type', 'city', 'name', 'coordinates', 'description']
