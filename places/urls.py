from django.urls import path
from django.views.generic import TemplateView

from . import views

app_name = 'places'
urlpatterns = [
    path('mapseconds/', views.MapSecondsView.as_view(), name='place_map_seconds'),
    path('mapstocks/', views.MapStockView.as_view(), name='place_map_stocks'),
    path('places/second/<int:pk>/', views.SecondDetailView.as_view(), name='second_detail'),
    path('places/stock/<int:pk>/', views.StockDetailView.as_view(), name='stock_detail'),
    # todo change with nothing
    path('places/', views.SecondListView.as_view(), name='place_all'),
    path('places/second/', views.SecondListView.as_view(), name='all_seconds'),
    path('places/stock', views.StockListView.as_view(), name='all_stocks'),
    path('places/suggest/', views.SuggestPlacetView.as_view(), name='place_suggest'),
    ]
