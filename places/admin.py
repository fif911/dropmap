from django.contrib import admin
from image_cropping import ImageCroppingMixin

from places.models import Second, Stock, City, Schedule, SuggetionPlace


class ScheduleInline(admin.TabularInline):
    model = Schedule


class SecondAdmin(ImageCroppingMixin, admin.ModelAdmin):
    # fieldsets = [
    #     (None,               {'fields': ['question_text']}),
    #     ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    # ]
    inlines = [ScheduleInline]
    list_display = ('name', 'address', 'day', 'show_on_map')
    list_filter = ['city__name', 'owner', 'show_on_map']
    search_fields = ['name', 'address']
    readonly_fields = ('owner',)

    def save_model(self, request, obj, form, change):
        obj.owner = request.user
        super().save_model(request, obj, form, change)

    # Display only objects which user has created
    # def queryset(self, request):
    #     request_user = request.user
    #     return Post.objects.filter(author=request_user)


class StockAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('name', 'address','sale', 'show_on_map')
    list_filter = ['city__name', 'owner','show_on_map']
    search_fields = ['name', 'address']
    readonly_fields = ('owner',)

    def save_model(self, request, obj, form, change):
        obj.owner = request.user
        super().save_model(request, obj, form, change)


class SuggetionPlaceAdmin(admin.ModelAdmin):
    list_display = ('name', 'checked', 'type')


admin.site.register(Second, SecondAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(City)
admin.site.register(SuggetionPlace, SuggetionPlaceAdmin)
# admin.site.register(Schedule)
