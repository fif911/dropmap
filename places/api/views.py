from django.http import Http404
from rest_framework import permissions, viewsets
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView, get_object_or_404
from rest_framework.response import Response

from places.models import Second, City, Stock
from .serializers import PlaceSerializer, CitySerializer


class PlaceListAPIView(ListAPIView):
    serializer_class = PlaceSerializer

    def get_queryset(self):
        """
        This view should return a list of all models by
        the city passed in the URL
        """

        city_slug = self.kwargs['city_slug']
        if self.kwargs['model'] == "second":
            list = Second.objects.filter(city__slug=city_slug).filter(show_on_map=True)
        else:
            list = Stock.objects.filter(city__slug=city_slug).filter(show_on_map=True)

        if list:
            return list
        else:
            raise Http404


class PlaceSingleAPIView(viewsets.ViewSet):
    def retrieve(self, request, model=None, id=None):
        if model == "second":
            place = get_object_or_404(Second, id=id)
        else:
            place = get_object_or_404(Stock, id=id)
        serializer = PlaceSerializer(place)
        return Response(serializer.data)


class CityViewSet(viewsets.ViewSet):
    """
    A ViewSet for listing or retrieving cities.
    """

    def list(self, request):
        queryset = City.objects.all()
        serializer = CitySerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, city_slug=None):
        city = get_object_or_404(City, slug=city_slug)
        serializer = CitySerializer(city)
        return Response(serializer.data)
