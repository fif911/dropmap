from django.urls import path
from django.views.generic import TemplateView

from .views import (
    PlaceListAPIView,
    CityViewSet, PlaceSingleAPIView)

app_name = 'places-api'
urlpatterns = [
    # path('', TemplateView.as_view(template_name="home/index.html")),
    # path('', views.IndexView.as_view(), name='index'),
    # path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # path('<int:question_id>/vote/', views.vote, name='vote'),

    # path('get_by_city/<model>/<slug:city_slug>', PlaceListAPIView.as_view(), name='places_api_on_map'),
    # todo get by second / stock name
    path('get_by_city/<model>/<slug:city_slug>', PlaceListAPIView.as_view(), name='places_api_on_map'),
    path('get/<model>/<id>', PlaceSingleAPIView.as_view({'get': 'retrieve'}), name='places_api_on_map'),
    path('get_cities/<slug:city_slug>', CityViewSet.as_view({'get': 'retrieve'}), name='place_api_get_city'),
    path('get_cities', CityViewSet.as_view({'get': 'list'}), name='place_api_get_cities'),
]
