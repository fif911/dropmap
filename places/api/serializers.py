from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from places.models import Second, City


# {
#     "type": "Feature",
#     "properties": {
#         "name": "Євро секонд хенд",
#         "day": "tuesday",
#         "url": "/iteminshop",
#         "adress": "Володимира Великого 59 A",
#         "photo_url": "../img/second_item.jpg",
#         "description": ""
#     },
#     "geometry": {
#         "type": "Point",
#         "coordinates": [24.000009, 49.807876]
#     }
# }
class NestedGeometrySerializer(serializers.Serializer):
    type = serializers.ReadOnlyField(default='Point')
    # coordinates = serializers.ReadOnlyField(default='[2,3]')
    coordinates = serializers.ListField(source='get_coordinates')
    #     child=serializers.IntegerField(min_value=0, max_value=100)
    #     # child2=serializers.IntegerField(min_value=0, max_value=100)
    # )


class NestedPropertiesSerializer(serializers.Serializer):
    name = serializers.CharField(source='get_name')
    day = serializers.CharField(source='get_day')
    sale = serializers.CharField(source='get_sale')
    url = serializers.CharField(source='get_absolute_url')
    address = serializers.CharField(source='get_address')
    photo_url = serializers.CharField(source='get_map_photo_url')
    description = serializers.CharField(source='get_description')


# todo add akcia field
class PlaceSerializer(ModelSerializer):
    type = serializers.ReadOnlyField(default='Feature')
    # city = serializers.SlugRelatedField(
    #     many=False,
    #     read_only=True,
    #     slug_field='name'
    # )
    properties = NestedPropertiesSerializer(source='*')
    geometry = NestedGeometrySerializer(source='*')

    class Meta:
        # ! important. this serializer is related to Second model. They shouldn't depend a lot
        model = Second
        fields = [
            'type',
            'properties',
            'geometry',
        ]


class CitySerializer(ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'
