from sorl.thumbnail import get_thumbnail
from unidecode import unidecode
from django.conf import settings
from django.core.validators import MaxValueValidator
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from geoposition.fields import GeopositionField
from django.templatetags.static import static

# todo set desc
NO_DESC_TEXT_MAP = "Інфи нема. Будеш там, дай знати, яка там сітуейшн\nInstagram: <a href='https://www.instagram.com/dropmap' target='_blank'>@dropmap</a>"
# NO_DESC_TEXT_MAP = None

PLACE_TYPES = (
    ("SC", "Секонд"),
    ("ST", "Сток"),
)
DAYS = (
    ("everyday", "Кожен день"),
    ("monday", "Понеділок"),
    ("tuesday", "Вівторок"),
    ("wednesday", "Середа"),
    ("thursday", "Четверг"),
    ("friday", "П'ятниця"),
    ("saturday", "Субота"),
    ("sunday", "Неділя"),
)
User = settings.AUTH_USER_MODEL


def upload_location(instance, filename):
    if instance._meta.object_name == "Stock":
        return "stocks/%s/%s" % (slugify(instance.name, allow_unicode=True), filename)
    if instance._meta.object_name == "Second":
        return "seconds/%s/%s" % (slugify(instance.name, allow_unicode=True), filename)


# Create your models here.
class City(models.Model):
    name = models.CharField(max_length=30, unique=True)
    slug = models.CharField(max_length=30, blank=True)
    longitude = models.FloatField()
    latitude = models.FloatField()
    boundaries = models.FloatField()

    def save(self, *args, **kwargs):
        self.slug = slugify(unidecode(self.name))
        super(City, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Second(models.Model):
    city = models.ForeignKey(City, default=1, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    coordinates = GeopositionField()
    # to get coor obj.coordinates.latitude & longitude
    day = models.CharField(max_length=10, choices=DAYS, default=DAYS[0], verbose_name="День оновлення")
    address = models.CharField(max_length=60)
    rating = models.PositiveSmallIntegerField(default=0, validators=[MaxValueValidator(5)])
    description = models.TextField(max_length=256, blank=True, null=True)

    # details
    phone = models.CharField(max_length=13, blank=True, null=True, help_text="Вводь телефон у форматі 099 000 00 00")
    shop_site = models.CharField(max_length=100, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    # main image original & cropping
    main_image = ProcessedImageField(upload_to=upload_location, blank=True, null=True,
                                     format='JPEG',
                                     options={'quality': 80}, verbose_name="Головне фото", help_text="Зазвичай входу")
    # list_page_cropping = ImageSpecField(source='main_image',
    #                                     processors=[ResizeToFill(230, 200)],
    #                                     format='JPEG',
    #                                     options={'quality': 80})
    # detail_page_cropping = ImageSpecField(source='main_image',
    #                                       processors=[ResizeToFill(350, 250)],
    #                                       format='JPEG',
    #                                       options={'quality': 80})
    # map_cropping = ImageSpecField(source='main_image',
    #                               processors=[ResizeToFill(50, 50)],
    #                               format='JPEG',
    #                               options={'quality': 50})

    # additional images
    image2 = ProcessedImageField(upload_to=upload_location,
                                 processors=[ResizeToFill(350, 250)],
                                 format='JPEG',
                                 options={'quality': 80}, blank=True, null=True)
    image3 = ProcessedImageField(upload_to=upload_location,
                                 processors=[ResizeToFill(350, 250)],
                                 format='JPEG',
                                 options={'quality': 80}, blank=True, null=True)

    show_on_map = models.BooleanField(default=True)
    owner = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        # return reverse('places:place_detail', kwargs={'slug': self.slug})
        return reverse('places:second_detail', kwargs={'pk': self.id})

    # getters for serializer
    def get_name(self):
        return self.name

    def get_day(self):
        return self.day

    def get_sale(self):
        return None

    def get_address(self):
        return self.address

    def get_map_photo_url(self):
        # todo set default image
        if self.main_image:
            image = get_thumbnail(self.main_image, '50x50', crop='center', quality=90)
            return image.url
        else:
            # todo or return nothing
            image = static('img/no-image50x50.jpg')
            return image

    def get_coordinates(self):
        return [self.coordinates.longitude, self.coordinates.latitude]

    def get_description(self):
        return self.description if self.description else NO_DESC_TEXT_MAP


class Stock(models.Model):
    city = models.ForeignKey(City, default=1, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    coordinates = GeopositionField()
    # to get coor obj.coordinates.latitude & longitude

    address = models.CharField(max_length=60)
    rating = models.PositiveSmallIntegerField(default=0, validators=[MaxValueValidator(5)])
    sale = models.CharField(max_length=60, blank=True, null=True)

    description = models.TextField(max_length=256, blank=True, null=True)

    # details
    phone = models.CharField(max_length=13, blank=True, null=True)
    shop_site = models.CharField(max_length=100, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    # main image original & cropping
    main_image = ProcessedImageField(upload_to=upload_location, blank=True, null=True,
                                     format='JPEG',
                                     options={'quality': 80}, verbose_name="Головне фото", help_text="Зазвичай входу")

    # additional images
    image2 = ProcessedImageField(upload_to=upload_location,
                                 processors=[ResizeToFill(350, 250)],
                                 format='JPEG',
                                 options={'quality': 80}, blank=True, null=True)
    image3 = ProcessedImageField(upload_to=upload_location,
                                 processors=[ResizeToFill(350, 250)],
                                 format='JPEG',
                                 options={'quality': 80}, blank=True, null=True)

    show_on_map = models.BooleanField(default=True)
    owner = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('places:stock_detail', kwargs={'pk': self.id})

    # getters for serializer
    def get_name(self):
        return self.name

    def get_day(self):
        return None

    def get_sale(self):
        return self.sale if self.sale else ""

    def get_address(self):
        return self.address

    def get_detail_photo_url(self):
        # todo set default image
        return self.detail_page_cropping.url if self.main_image else static('img/no-image.png')

    def get_map_photo_url(self):
        if self.main_image:
            image = get_thumbnail(self.main_image, '50x50', crop='center', quality=90)
            return image.url
        else:
            image = static('img/no-image50x50.jpg')
            return image

    def get_list_photo_url(self):
        # todo set default image
        return self.list_page_cropping.url if self.main_image else static('img/no-image50x50.jpg')

    def get_coordinates(self):
        return [self.coordinates.longitude, self.coordinates.latitude]

    def get_description(self):
        return self.description if self.description else NO_DESC_TEXT_MAP


class SuggetionPlace(models.Model):
    type = models.CharField(max_length=2, choices=PLACE_TYPES, default=PLACE_TYPES[0])
    city = models.ForeignKey(City, default=1, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    coordinates = GeopositionField()
    description = models.CharField(max_length=256, blank=True, null=True)
    checked = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Schedule(models.Model):
    city = models.OneToOneField(Second, on_delete=models.CASCADE, primary_key=True)
    mon = models.PositiveSmallIntegerField(blank=True, null=True)
    tue = models.PositiveSmallIntegerField(blank=True, null=True)
    wed = models.PositiveSmallIntegerField(blank=True, null=True)
    thu = models.PositiveSmallIntegerField(blank=True, null=True)
    fri = models.PositiveSmallIntegerField(blank=True, null=True)
    sat = models.PositiveSmallIntegerField(blank=True, null=True)
    sun = models.PositiveSmallIntegerField(blank=True, null=True)
