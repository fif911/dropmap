"""
Django settings for dropmap project.

Generated by 'django-admin startproject' using Django 2.2.1.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
from easy_thumbnails.conf import Settings as thumbnail_settings

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'c&#b^($9!z5j-n8t1xn0th#6*y$lmwbdia_(c++81!^!2&(xe8'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# todo uncomment in prod
# ALLOWED_HOSTS = ['dropmap.com.ua', 'dropmap-project.herokuapp.com']
ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # apps
    "home.apps.HomeConfig",
    "places.apps.PlacesConfig",
    "about.apps.AboutConfig",
    "shop.apps.ShopConfig",
    "user.apps.UserConfig",
    # third party
    "geoposition",
    'rest_framework',

    # 'easy_thumbnails',
    'image_cropping',

    'sorl.thumbnail',
    # todo check what is needed for imagekit & sorl_thumbnailsdelete
    'imagekit',
    'django_cleanup',
    'django_filters',
    # for amazon storage
    'storages',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'dropmap.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'dropmap.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/




# Redirect to home URL after login (Default redirects to /accounts/profile/)
LOGIN_REDIRECT_URL = '/'
AUTH_USER_MODEL = 'user.User'
# admin picker geo
GEOPOSITION_GOOGLE_MAPS_API_KEY = 'AIzaSyBlUgR1k7OGtyj5DVomB3DYA739xE68fTA'
GEOPOSITION_MAP_OPTIONS = {
    'minZoom': 5,
    'zoom': 13,
    'center': {'lat': 49.83968, 'lng': 24.0297},
    # 'scrollwheel': True,
    # 'maxZoom': 15,
}

GEOPOSITION_MARKER_OPTIONS = {
    'cursor': 'move',
    'position': {'lat': 49.83968, 'lng': 24.0297},
    # 'position':{'lat': 52.5, 'lng': 13.4}

}
# END admin picker geo

THUMBNAIL_PROCESSORS = (
                           'image_cropping.thumbnail_processors.crop_corners',
                       ) + thumbnail_settings.THUMBNAIL_PROCESSORS

# EMAIL SETTINGS
EMAIL_HOST = 'smtp.eu.mailgun.org'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'postmaster@mg.dropmap.com.ua'
EMAIL_HOST_PASSWORD = '890de5a49d1e951ebd7bf85925d98a3e-4167c382-b0f4d4a7'
EMAIL_USE_TLS = True

STATIC_URL = '/static/'

try:
    from .local_settings import *
except ImportError:
    # disable collect static
    # heroku config:set DISABLE_COLLECTSTATIC=1
    # RUN IT ON HEROKU python manage.py collectstatic -i geoposition
    STATIC_ROOT = os.path.join(BASE_DIR, "static")

    # AMAZON S3 media only

    AWS_LOCATION = 'static'
    AWS_ACCESS_KEY_ID = 'AKIAZIA7QM4TFZWP6I4N'
    AWS_SECRET_ACCESS_KEY = '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'
    AWS_STORAGE_BUCKET_NAME = 'dropmap-assets'
    AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
    AWS_S3_OBJECT_PARAMETERS = {
        'CacheControl': 'max-age=86400',
    }
    DEFAULT_FILE_STORAGE = 'dropmap.storage_backends.MediaStorage'

    AWS_DEFAULT_ACL = None
    THUMBNAIL_FORCE_OVERWRITE = True

# DB url for heroku

##########################
# HAVE TO BE ADDDED TO REQ psycopg2 !!!!!!!
# or install Postgres locally

import dj_database_url

db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)
