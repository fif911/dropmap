from django.db import models
from django.utils.text import slugify

PURPOSES = (
    ("General question", "Загальне питання"),
    ("Error", "Баг"),
    ("Suggestion", "Пропозиція"),
    ("Another", "Інше"),
)


def upload_location(instance, filename):
    return "message/%s/%s" % (slugify(instance.purpose, allow_unicode=True), filename)


# Create your models here.
class Message(models.Model):
    purpose = models.CharField(max_length=50, choices=PURPOSES, default=PURPOSES[0])
    text = models.CharField(max_length=256)
    photo = models.ImageField(upload_to=upload_location, blank=True, null=True)
    name = models.CharField(max_length=50, verbose_name="Твої контакти", blank=True, null=True)
    viewed = models.BooleanField(default=False)

    def __str__(self):
        return self.purpose
