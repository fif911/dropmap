from django.contrib import admin

# Register your models here.
from about.models import Message

class MessageAdmin(admin.ModelAdmin):

    list_display = ('purpose', 'viewed', 'text')
    list_filter = ['viewed']
    # search_fields = ['name', 'author__username']

admin.site.register(Message,MessageAdmin)