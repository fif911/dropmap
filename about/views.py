from django.views.generic import CreateView

from about.models import Message


class AboutView(CreateView):
    template_name = "about/about.html"
    model = Message
    success_url = "/"
    fields = ['purpose', 'text', 'name', 'photo']

# SECOND CREATE FOR BETA TESTERS

# class AuthorCreate(LoginRequiredMixin, CreateView):
#     model = Author
#     fields = ['name']
#
#     def form_valid(self, form):
#         form.instance.created_by = self.request.user
#         return super().form_valid(form)
