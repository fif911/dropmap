from django.views import generic
from places.models import Second
from shop.models import Item
from datetime import datetime

class HomeView(generic.TemplateView):
    template_name = "home/home.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # todo filter by current day
        today = datetime.today().strftime('%A').lower()
        print(today)
        context['second_list'] = Second.objects.filter(day = today).order_by('-rating', '?')[:4]
        context['item_list'] = Item.objects.filter(status='AVAILABLE').filter(on_site=True).order_by('-promoting', '-id')[:6]
        return context
