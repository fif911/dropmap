from django.db import models
from django.contrib.auth.models import AbstractUser

from shop.models import Item

LOCATIONS = (
    ("LV", "Львів"),
    ("IF", "Івано-Франківськ"),
    ("TP", "Тернопіль"),
    ("KV", "Київ"),
    ("EL", "Десь далеко"),
)


class User(AbstractUser):

    # todo location choose from City model
    # psycopg2.errors.StringDataRightTruncation: value too long for type character varying(4)
    location = models.CharField(max_length=4, choices=LOCATIONS, default="LV")
    is_author_proved = models.BooleanField(default=False,
                                           help_text="Не анонімний продаж. Додає такі можливості як особиста зустріч")
    personal_meeting = models.BooleanField(default=True, help_text="Згода на особисті зустрічі")
    liked_items = models.ManyToManyField(Item, blank=True)
    # delivery info
    pib = models.CharField(max_length=255, blank=True, null=True, verbose_name="ПІБ")
    city = models.CharField(max_length=30, blank=True, null=True, verbose_name="Твоє місто")
    address = models.CharField(max_length=255, blank=True, null=True, verbose_name="Адреса")
    phone = models.CharField(max_length=10, blank=True, null=True, verbose_name="Номер телефону")
