from django.urls import path

from user.forms import UserUpdateDeliveryInfoForm, UserUpdateBuyInfoForm
from . import views

app_name = 'user'
urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('profile/', views.ProfileView.as_view(), name='profile'),
    path('profile/delivery-info/<int:pk>', views.UserDelInfoUpdateView.as_view(form_class=UserUpdateDeliveryInfoForm), name='delivery-info'),
    path('profile/buy-info/<int:pk>', views.UserDelInfoUpdateView.as_view(form_class=UserUpdateBuyInfoForm), name='buy-info'),
    path('profile/my-favourite/', views.UserLikedView.as_view(), name='my-favourite'),
    # path('profile/', views.signup, name='signup'),
]
