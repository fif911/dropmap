from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.forms import ModelForm

User = get_user_model()

class BaseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'textbox__control'

class SignUpForm(UserCreationForm, BaseForm):
    # todo add clasees to fix view
    username = forms.CharField(max_length=60, label="Instagram Username")

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2',)

class UserUpdateBuyInfoForm(BaseForm):
    class Meta:
        model = User
        fields = ['location', 'email']

class UserUpdateDeliveryInfoForm(BaseForm):
    # todo work around with shipping info

    class Meta:
        model = User
        # Custom fields
        fields = ['pib', 'city', 'address', 'email', 'phone']
        # labels, help_texts and error_messages
        labels = {
            "email": "Твій Email"
        }
        # labels = {
        #     'name': _('Writer'),
        # }
        # help_texts = {
        #     'name': _('Some useful help text.'),
        # }
        # error_messages = {
        #     'name': {
        #         'max_length': _("This writer's name is too long."),
        #     },
        # }

    def clean(self):
        # data from the form is fetched using super function
        super(UserUpdateDeliveryInfoForm, self).clean()

        # extract the username and text field from the data
        pib = self.cleaned_data.get('pib')
        city = self.cleaned_data.get('city')
        address = self.cleaned_data.get('address')
        email = self.cleaned_data.get('email')
        phone = self.cleaned_data.get('phone')

        # todo add normal validators

        if not pib or len(pib) < 5:
            self._errors['pib'] = self.error_class([
                'Minimum 5 characters required'])
        if not city or len(city) < 1:
            self._errors['pib'] = self.error_class([
                'Minimum 2 characters required'])
        if not address or len(address) < 5:
            self._errors['pib'] = self.error_class([
                'Minimum 5 characters required'])
        if not email or len(email) < 5:
            self._errors['pib'] = self.error_class([
                'Minimum 5 characters required'])
        if not phone or len(phone) < 5:
            self._errors['pib'] = self.error_class([
                'Minimum 5 characters required'])

        return self.cleaned_data
