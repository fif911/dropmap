from django.contrib.auth import login, authenticate
# from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, redirect
from django.views.generic import DetailView, TemplateView, UpdateView
from shop.models import Item
from user.forms import SignUpForm, UserUpdateDeliveryInfoForm
from django.contrib.auth import get_user_model

User = get_user_model()


class UserDelInfoUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    '''Simply view for updationg info in profile module'''

    model = User
    login_url = '/login/'
    template_name = "user/user_update.html"
    success_url = '#'
    success_message = 'Інформація збережена'
    form_class = None
    def get_form_class(self):
        return self.form_class


class UserLikedView(LoginRequiredMixin, TemplateView):
    template_name = 'user/author_liked.html'
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['user_liked_items'] = User.objects.get(username=self.request.user).liked_items.all()
        return context


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'user/profile.html'
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['user'] = User.objects.get(username=self.request.user)
        context['current_items'] = Item.objects.filter(author__username=self.request.user).filter(
            on_site=True).filter(status="AVAILABLE").order_by('-created')
        context['to_check_items'] = Item.objects.filter(author__username=self.request.user).filter(
            on_site=True).filter(status="TO_CHECK").order_by('-created')
        context['promoting_items'] = Item.objects.filter(author__username=self.request.user).filter(
            on_site=True).filter(status="AVAILABLE").filter(promoting=True).order_by('-created')
        return context


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('user:profile')
    else:
        if request.user.is_authenticated:
            return redirect('home:home')
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})
