from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from user.models import User


class UserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('DropMap user settings'),
         {'fields': ('location', 'is_author_proved', 'personal_meeting', 'liked_items')}),
        (('DropMap delivery info'), {'fields': ('pib', 'city', 'address', 'phone')}),
        (('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    # fieldsets = UserAdmin.fieldsets + (
    #         (None, {'fields': ('phone',)}),
    # )


# admin.site.register(MyUser, MyUserAdmin)

admin.site.register(User, UserAdmin)
