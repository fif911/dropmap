# Dropmap
It's project about seconds, stocks and good life

TODO install heroku & test push on it

## How to clean JS (url and api to normal)
1 find & delete all base_url = in the code

2 set: var base_url = window.location.origin;

3 delete locations & location fake vars

4 uncomment api call lines request_locations, request_location, request_city
```bash
# urls for api calls
# get city
request_city.open('GET', base_url + "api/places/get_cities/lviv?format=json", true);
# seconds
request_locations.open('GET', base_url + 'api/places/get_by_city/second/lviv?format=json', true);
# stocks
request_locations.open('GET', base_url + 'api/places/get_by_city/stock/lviv?format=json', true);
# get single location
request_location.open('GET', base_url + "api/places/get/"+ api_request_postfix+"?format=json", true);
# IMPORTANT while not fixed on single place detail CHANGE to:  ("var" added)
var location = JSON.parse(this.response);
```
5 enjoy

## Push to Heroku

1 Disable collect static on heroku
```bash
heroku config:set DISABLE_COLLECTSTATIC=1
```
2 ensure that psycopg2==2.8.3 is added to requirements.txt
```bash
psycopg2==2.8.3
```
3 To push run commands
```bash
git add .
git commit -m ""
git push origin master
sandrrichfif911@gmail.com
{{password}}
git push heroku master && heroku run python manage.py collectstatic -i geoposition --noinput
# if you need to migrate
heroku run python manage.py migrate
```
4 to check dyno usage
```bash
heroku ps -a dropmap-project
```
5 show migrations on server
```bash
heroku run python manage.py showmigrations
```

## License
[MIT](https://choosealicense.com/licenses/mit/)