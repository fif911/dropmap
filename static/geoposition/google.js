var is_django = true;
if (jQuery != undefined) {
    if (django == undefined) {
        var django = {
            'jQuery': jQuery,
        };
        is_django = false
    } else {
        if (django['jQuery'] == undefined) {
            django['jQuery'] = jQuery;

        }

    }
}
console.log("is_django " + is_django);

function addYourLocationButton(map) {
    var controlDiv = document.createElement('div');

    var firstChild = document.createElement('div');
    firstChild.style.backgroundColor = '#fff';
    firstChild.style.border = 'none';
    firstChild.style.outline = 'none';
    firstChild.style.width = '28px';
    firstChild.style.height = '28px';
    firstChild.style.borderRadius = '2px';
    firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
    firstChild.style.cursor = 'pointer';
    firstChild.style.marginRight = '10px';
    firstChild.style.padding = '0';
    firstChild.title = 'Your Location';
    controlDiv.appendChild(firstChild);

    var secondChild = document.createElement('div');
    secondChild.style.position = 'absolute';
    secondChild.style.margin = '5px';
    secondChild.style.width = '18px';
    secondChild.style.height = '18px';
    secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-2x.png)';
    secondChild.style.backgroundSize = '180px 18px';
    secondChild.style.backgroundPosition = '0 0';
    secondChild.style.backgroundRepeat = 'no-repeat';
    firstChild.appendChild(secondChild);

    google.maps.event.addListener(map, 'center_changed', function () {
        secondChild.style['background-position'] = '0 0';
    });

    firstChild.addEventListener('click', function () {
        var imgX = 0,
            animationInterval = setInterval(function () {
                imgX = -imgX - 18;
                secondChild.style['background-position'] = imgX + 'px 0';
            }, 500);

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(latlng);
                map.setZoom(17);
                // draw circle here
                clearInterval(animationInterval);
                secondChild.style['background-position'] = '-144px 0';
            },
            function error(msg) {alert('Дозволь доступ до своєї геолокації щоб використовувати цю функцію');},
            {maximumAge:10000, timeout:5000, enableHighAccuracy: true}
            );
        } else {
            clearInterval(animationInterval);
            secondChild.style['background-position'] = '0 0';
        }
    });

    controlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);
}


(function ($) {

    $(document).ready(function () {
        try {
            var _ = google; // eslint-disable-line no-unused-vars
        } catch (ReferenceError) {
            console.log('geoposition: "google" not defined. You might not be connected to the internet.');
            return;
        }

        var mapDefaults = {
            'mapTypeId': google.maps.MapTypeId.ROADMAP,
            // 'scrollwheel': false,
            'streetViewControl': false,
            // 'panControl': false,
            'gestureHandling': 'cooperative'
        };

        var markerDefaults = {
            'draggable': true,
            // 'animation': google.maps.Animation.DROP
        };
        console.log(django);
        $('.geoposition-widget').each(function () {
            var $container = $(this),
                $mapContainer = $('<div class="geoposition-map" />'),
                $addressRow = $('<div class="geoposition-address" />'),
                $searchRow = $('<div class="geoposition-search" />'),
                $searchInput = $('<input>', {'type': 'text', 'placeholder': "Input an address"}),
                // $searchInput = $('<input>', {'type': 'text', 'placeholder': django.gettext('Input an address...')}),
                $latitudeField = $container.find('input.geoposition:eq(0)'),
                $longitudeField = $container.find('input.geoposition:eq(1)'),
                $currentCity = $("#id_city option:selected").text(),
                latitude = parseFloat($latitudeField.val()) || null,
                longitude = parseFloat($longitudeField.val()) || null,
                map,
                mapLatLng,
                mapOptions,
                mapCustomOptions,
                markerOptions,
                markerCustomOptions,
                marker;

            // TODO MAKE FUCNCTION
            // console.log($currentCity);
            // if(!(latitude && longitude)) {
            //
            //     $searchInput.val($currentCity);
            //     doSearch();
            // }


            $mapContainer.css('height', $container.attr('data-map-widget-height') + 'px');
            mapCustomOptions = JSON.parse($container.attr('data-map-options'));
            markerCustomOptions = JSON.parse($container.attr('data-marker-options'));

            function doSearch() {
                var gc = new google.maps.Geocoder();
                $mapContainer.parent().find('ul.geoposition-results').remove();
                gc.geocode({
                    'address': $searchInput.val()
                }, function (results, status) {
                    if (status == 'OK') {
                        var updatePosition = function (result) {
                            if (result.geometry.bounds) {
                                map.fitBounds(result.geometry.bounds);
                            } else {
                                map.panTo(result.geometry.location);
                                map.setZoom(18);
                            }
                            marker.setPosition(result.geometry.location);
                            google.maps.event.trigger(marker, 'dragend');
                        };
                        if (results.length == 1) {
                            updatePosition(results[0]);
                        } else {
                            var $ul = $('<ul />', {'class': 'geoposition-results'});
                            $.each(results, function (i, result) {
                                var $li = $('<li />');
                                $li.text(result.formatted_address);
                                $li.bind('click', function () {
                                    updatePosition(result);
                                    $li.closest('ul').remove();
                                });
                                $li.appendTo($ul);
                            });
                            $mapContainer.after($ul);
                        }
                    }
                });
            }

            function doGeocode() {
                var gc = new google.maps.Geocoder();
                gc.geocode({
                    'latLng': marker.position
                }, function (results) {
                    $addressRow.text('');
                    if (results && results[0]) {
                        $addressRow.text(results[0].formatted_address);
                    }
                });
            }

            // custom function for auto select
            // do not pass if not django e.g. in forms
            var passFirstInt = is_django ? 0 : 1;
            // console.log("passFirstInt " +passFirstInt);
            $("#id_city").bind("change", function (e) {
                if (passFirstInt) {
                    $currentCity = $("#id_city option:selected").text();
                    console.log($currentCity);
                    $searchInput.val($currentCity);
                    doSearch()
                } else passFirstInt = 1;
            });

            var autoSuggestTimer = null;
            $searchInput.bind('keydown', function (e) {
                if (autoSuggestTimer) {
                    clearTimeout(autoSuggestTimer);
                    autoSuggestTimer = null;
                }

                // if enter, search immediately
                if (e.keyCode == 13) {
                    e.preventDefault();
                    doSearch();
                } else {
                    // otherwise, search after a while after typing ends
                    autoSuggestTimer = setTimeout(function () {
                        doSearch();
                    }, 1000);
                }
            }).bind('abort', function () {
                $(this).parent().find('ul.geoposition-results').remove();
            });
            $searchInput.appendTo($searchRow);
            $container.append($searchRow, $mapContainer, $addressRow);

            mapLatLng = new google.maps.LatLng(latitude, longitude);

            mapOptions = $.extend({}, mapDefaults, mapCustomOptions);

            if (!(latitude === null && longitude === null && mapOptions['center'])) {
                mapOptions['center'] = mapLatLng;
            }

            if (!mapOptions['zoom']) {
                mapOptions['zoom'] = latitude && longitude ? 15 : 1;
            }

            map = new google.maps.Map($mapContainer.get(0), mapOptions);
            markerOptions = $.extend({}, markerDefaults, markerCustomOptions, {
                'map': map
            });
            if (!(latitude === null && longitude === null && markerOptions['position'])) {
                markerOptions['position'] = mapLatLng;
            }

            marker = new google.maps.Marker(markerOptions);
            addYourLocationButton(map);

            map.addListener('click', function (event) {
                marker.setMap(null);
                markerOptions['position'] = event.latLng;
                marker = new google.maps.Marker(markerOptions);
                google.maps.event.addListener(marker, 'dragend', function () {
                    $latitudeField.val(this.position.lat());
                    $longitudeField.val(this.position.lng());
                    doGeocode();
                });
                doGeocode();

            });
            google.maps.event.addListener(marker, 'dragend', function () {
                $latitudeField.val(this.position.lat());
                $longitudeField.val(this.position.lng());
                doGeocode();
            });
            if ($latitudeField.val() && $longitudeField.val()) {
                google.maps.event.trigger(marker, 'dragend');
            }

            $latitudeField.add($longitudeField).bind('keyup', function () {
                var latitude = parseFloat($latitudeField.val()) || 0;
                var longitude = parseFloat($longitudeField.val()) || 0;
                var center = new google.maps.LatLng(latitude, longitude);
                map.setCenter(center);
                map.setZoom(15);
                marker.setPosition(center);
                doGeocode();
            });
        });
    });
})(django.jQuery);
