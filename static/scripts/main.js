(function () {
    'use strict';

// *      Developed by Alex Zakotyanskiy  *//

// =============================================
// Modules navigation
// =============================================

    var base_url = window.location.origin+"/";
    console.log(base_url);

    var burger_menu = document.querySelector('.hamburger-link');
    if (burger_menu) {
        burger_menu.addEventListener('click', function () {
            this.classList.toggle('active');
            var menu = document.querySelector('.o-header__menu');
            menu.classList.toggle('active');
            document.querySelector('body').classList.toggle('active');

            var div_background = document.createElement('div');
            div_background.className = 'div-background';
            if (menu.classList.contains('active')) {
                menu.appendChild(div_background);
                div_background.addEventListener('click', function () {
                    document.querySelector('.hamburger-link').classList.toggle('active');
                    document.querySelector('.o-header__menu').classList.toggle('active');
                    document.querySelector('body').classList.toggle('active');
                    document.querySelector('.o-header__menu').removeChild(document.querySelector('.div-background'));
                });
            } else {
                document.querySelector('.o-header__menu').removeChild(document.querySelector('.div-background'));
            }
        });
    }

    function addScrollClass() {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        var burger_menu = document.querySelector('.o-header__navbar');
        if (burger_menu) {
            if (scrolled > 90) {
                // burger_menu.style.backgroundColor = '#00132B';
                burger_menu.classList.add("scrolled");
            } else {
                // burger_menu.style.backgroundColor = 'transparent';
                burger_menu.classList.remove("scrolled");
            }
        }
    }

    document.addEventListener('DOMContentLoaded', addScrollClass);

    window.onscroll = addScrollClass;

// delete it
    document.addEventListener('click', function (event) {
        console.log(event.target);
    });

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var html;
    var body;

    function scrollToTop(totalTime, easingPower) {
        var timeInterval = 1; //in ms
        var scrollTop = Math.round(body.scrollTop || html.scrollTop);
        var timeLeft = totalTime;
        var scrollByPixel = setInterval(function () {
            var percentSpent = (totalTime - timeLeft) / totalTime;
            if (timeLeft >= 0) {
                var newScrollTop = scrollTop * (1 - easeInOut(percentSpent, easingPower));
                body.scrollTop = newScrollTop;
                html.scrollTop = newScrollTop;
                timeLeft--;
            } else {
                clearInterval(scrollByPixel);
            }
        }, timeInterval);
    }

    function easeInOut(t, power) {
        if (t < 0.5) {
            return 0.5 * Math.pow(2 * t, power);
        } else {
            return 0.5 * (2 - Math.pow(2 * (1 - t), power));
        }
    }

// if(footerBut){}


    var Footer = function Footer() {
        _classCallCheck(this, Footer);

        html = document.documentElement;
        body = document.body;
        this.name = 'footer';
        // add if ?
        var footer_button = document.querySelector(".footer--bar__button");
        if (footer_button) {
            footer_button.addEventListener('click', function (e) {
                // if(e.target.closest('.footer--bar__button') ) {
                scrollToTop(300, 3);
                // }
            });
        }
    };

// =========================================
// Configuration
// =========================================
    var Config = {
        siteName: 'Drop Map',
        company: 'Yellow Leaf Technologies'
    };

    function _classCallCheck$1(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var DropDownSelect = function DropDownSelect() {
        _classCallCheck$1(this, DropDownSelect);

        this.name = 'drop-down-select';
        console.log('%s module', this.name.toLowerCase());

        var selectArray = document.querySelectorAll('.pseudo-choosed-item');
        var selectWrappers = document.querySelectorAll('.select-wrapper');

        var currentSelect = void 0;
        var selectOptions = void 0;

        for (var i = 0; i < selectWrappers.length; i++) {
            var wrapper = selectWrappers[i];

            wrapper.addEventListener('click', function (e) {
                // get pseudo select which is in wrapper
                var this_pseudo_select = this.querySelector('.pseudo-choosed-item');
                // close one by one
                for (var _i = 0; _i < selectArray.length; _i++) {
                    // if not current -> close
                    if (selectArray[_i] != this_pseudo_select) {
                        selectArray[_i].nextElementSibling.classList.remove('active');
                    }
                }

                // open current
                var select_list = this_pseudo_select.nextElementSibling;
                currentSelect = select_list;
                selectOptions = currentSelect.querySelectorAll('li');
                select_list.classList.toggle('active');
            });
        }

        // for (let i = 0; i < selectArray.length; i++) {
        //   let select = selectArray[i];
        //   // console.log(selectArray[i].nextElementSibling)
        //   select.addEventListener('click', function (e) {
        //     // if (this.nextElementSibling.classList.conta)
        //     for (let i = 0; i < selectArray.length; i++) {
        //       if (selectArray[i] != this){
        //         selectArray[i].nextElementSibling.classList.remove('active');
        //       }
        //     }
        //     let select_list = this.nextElementSibling;
        //     currentSelect = select_list;
        //     selectOptions = currentSelect.querySelectorAll('li')
        //     select_list.classList.toggle('active');
        //   })
        // }

        document.addEventListener('click', function (e) {
            if (selectOptions) {
                var _loop = function _loop(_i2) {
                    var option = selectOptions[_i2];

                    option.addEventListener('click', function (e) {
                        var select = currentSelect.nextElementSibling;
                        var choosed_item = this.parentNode.previousElementSibling;

                        this.closest('.pseudo-select-list').classList.remove('active');
                        select.querySelectorAll('option')[_i2].selected = true;

                        choosed_item.innerHTML = this.innerHTML + " ▾";
                    });
                };

                for (var _i2 = 0; _i2 < selectOptions.length; _i2++) {
                    _loop(_i2);
                }
            }
        });

        document.addEventListener('click', function (e) {
            if (!e.target.closest('.select-wrapper') || e.target.closest('.pseudo-select-list')) {
                var selects = document.querySelectorAll('.pseudo-select-list');

                for (var index = 0; index < selects.length; index++) {
                    var element = selects[index];
                    element.classList.remove("active");
                }
            }
        });
    };

    function _classCallCheck$2(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var MShopTopbar = function MShopTopbar() {
        _classCallCheck$2(this, MShopTopbar);

        this.name = 'm-shop-topbar';
        console.log('%s module', this.name.toLowerCase());
        // open & close FILTER BAR  & rotate arrow icon
        var filterBut = document.querySelector(".filter-button");
        if (filterBut) {
            filterBut.addEventListener('click', function () {
                var filterBar = document.querySelector(".drop-down-filter-bar");
                filterBar.classList.toggle('show');
                document.querySelector(".icon.arrow").classList.toggle('rotate180');
            });
        }
    };

    function _classCallCheck$3(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var PageSwitcher = function PageSwitcher() {
        _classCallCheck$3(this, PageSwitcher);

        // this.name = 'page-switcher';
        // console.log('%s module', this.name.toLowerCase());
        function moveToThisSwitcherLink() {
            console.log(this);
            var switcher = document.querySelector('.page-switcher--active');
            var linkCoordinates = this.getBoundingClientRect();

            switcher.style.left = linkCoordinates.left - 15 + 'px';
            switcher.style.width = linkCoordinates.width + 30 + 'px';
        }

        // returns undefined
        // let x = document.getElementsByClassName('link-second')[0]
        // console.log(x)
        // x.addEventListener('mouseover', activeSwitcher );
        var linkSecond = document.querySelector('.link-second');
        if (linkSecond) {
            var returnNotActiveEl = function returnNotActiveEl() {
                var links = document.querySelectorAll('.m-page-switcher');
                for (var i = 0; i < links.length; i++) {
                    if (!links[i].classList.contains('active')) {
                        // console.log(links[i]);
                        return links[i];
                    }
                }
            };

            var moveToActiveSwitcherLink = function moveToActiveSwitcherLink() {
                var activeLink = document.querySelector('.page-switcher a.active');
                var switcher = document.querySelector('.page-switcher--active');
                var linkCoordinates = activeLink.getBoundingClientRect();
                switcher.style.left = linkCoordinates.left - 15 + 'px';
                switcher.style.width = linkCoordinates.width + 30 + 'px';
            };
            // console.log(returnNotActiveEl());


            document.querySelector('.link-second').addEventListener('mouseover', moveToThisSwitcherLink);
            document.querySelector('.link-stock').addEventListener('mouseover', moveToThisSwitcherLink);

            returnNotActiveEl().addEventListener('mouseleave', moveToActiveSwitcherLink);
            // it cause error in footer scrolling couse window.onload func
            // ## TODO  window.onload all in 1 func
            // window.onload = moveToActiveSwitcherLink;
            document.addEventListener('DOMContentLoaded', moveToActiveSwitcherLink);
            window.addEventListener("resize", moveToActiveSwitcherLink);
        }
    };

// COLORS CONSTANTS FOR DAYS - SECONDS
// monday orange
// tuesday blue
// wednesday olive
// thursday dark-blue
// friday violet
// saturday red
// sunday yellow

    function _classCallCheck$4(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var OMap = function OMap() {
        _classCallCheck$4(this, OMap);

        this.name = 'o-map';
        console.log('%s module', this.name.toLowerCase());
        var mapfound = document.querySelector("#leafletmap");

        if (mapfound) {
            var activeDays;
            var lastGeoJSONlayer;
            var current_town;
            var locations;
            var mymap;
            var request_city;
            var request_locations;
            var GeoIconClass;
            var last_m, last_c;
            var GeoControl;
            var day;
            var weekday;
            var today;
            var tomorrow;
            var show_everyday_second;
            var MarkerIconClass;
            var orangeIcon, blueIcon, darkblueIcon, oliveIcon, violetIcon, yellowIcon, GreyIcon, redIcon;

            (function () {
                // ------------
                // Functions
                // ------------

                var createCustomPopup = function createCustomPopup(name, url, address, photo_url, description) {
                    // TODO set max characters to name
                    var customPopUp = '<div class="pop-up"><div class="header"><span class="popupCustomCloser">Close</span><p class="title"><b>' + name + '</b></p><a href="' + url + '">View</a></div><hr><div class="content"><p><img class="image" src="' + photo_url + '">' + description + '</p></div><hr class="hr-last"><div class="pop-upfooter"><p>' + address + '.</p></div></div>';
                    return customPopUp;
                };

                var onEachFeature = function onEachFeature(feature, layer) {
                    layer.bindPopup(createCustomPopup(feature.properties.name, feature.properties.url, feature.properties.address, feature.properties.photo_url, feature.properties.description));
                    layer.on('popupopen', function (popup) {
                        if (document.querySelectorAll(".popupCustomCloser").length > 1) {
                            document.getElementsByClassName("popupCustomCloser")[1].addEventListener('click', function () {
                                document.getElementsByClassName('leaflet-popup-close-button')[0].click();
                            });
                        } else {
                            document.getElementsByClassName("popupCustomCloser")[0].addEventListener('click', function () {
                                document.getElementsByClassName('leaflet-popup-close-button')[0].click();
                            });
                        }
                    });
                };

                // PSEUDO GLOBAL VARS


                // sort function
                var activeDaysFilter = function activeDaysFilter(feature, layer) {
                    console.log(arguments.length);
                    if (activeDays.includes(feature.properties.day)) return true;
                };

                // ----------------------
                // MARKERS
                // ----------------------

                // Defining an icon class


                var addToMapGeoJSON = function addToMapGeoJSON() {
                    var geoJSONlayer = L.geoJSON(locations, {
                        pointToLayer: function pointToLayer(feature, latlng) {
                            switch (feature.properties.day.toLowerCase()) {
                                case "everyday":
                                    return L.marker(latlng, {icon: GreyIcon});
                                case "monday":
                                    return L.marker(latlng, {icon: orangeIcon});
                                case "tuesday":
                                    return L.marker(latlng, {icon: blueIcon});
                                case "wednesday":
                                    return L.marker(latlng, {icon: oliveIcon});
                                case "thursday":
                                    return L.marker(latlng, {icon: darkblueIcon});
                                case "friday":
                                    return L.marker(latlng, {icon: violetIcon});
                                case "saturday":
                                    return L.marker(latlng, {icon: redIcon});
                                case "sunday":
                                    return L.marker(latlng, {icon: yellowIcon});
                            }
                        },
                        filter: activeDaysFilter,
                        onEachFeature: onEachFeature
                    }).addTo(mymap);

                    // mymap.fitBounds(geoJSONlayer.getBounds());
                    return geoJSONlayer;
                };

                activeDays = [];
                lastGeoJSONlayer = undefined;
                // default town lviv

                current_town = {
                    "id": 1,
                    "name": "Львів",
                    "slug": "lviv",
                    "longitude": 24.040532,
                    "latitude": 49.837955,
                    "boundaries": 0.2
                };
                locations = [];

                // GEOJSON GET POINTS IN TOWN


                // ----------------------
                // INIT MAP
                // ----------------------
                // todo change to map mymap to map & leafletmap to map

                // var mymap = L.map('leafletmap').setView([current_town.latitude,current_town.longitude], 12);

                mymap = L.map('leafletmap', {
                    gestureHandling: true
                }).setView([current_town.latitude, current_town.longitude], 12);

                L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    // cause of this map box tile
                    tileSize: 512,
                    zoomOffset: -1,

                    maxZoom: 18,
                    minZoom: 11,
                    id: 'mapbox.streets',
                    accessToken: 'pk.eyJ1IjoiZmlmOTExIiwiYSI6ImNqMHZidmc1azAwMDcycXRiM2tkbnNkcWQifQ.X65DkBqIYJURurRhpT1UJA'
                }).addTo(mymap);

                // ----------------------
                // API GETTING NESSESARY INFO
                // ----------------------

                // request CITY
                request_city = new XMLHttpRequest();

                request_city.open('GET', base_url + "api/places/get_cities/lviv?format=json", true);

                request_city.onload = function () {
                    current_town = JSON.parse(this.response);
                    if (request_city.status >= 200 && request_city.status < 400) {
                        mymap.setMaxBounds([[current_town.latitude - current_town.boundaries, current_town.longitude - current_town.boundaries], [current_town.latitude + current_town.boundaries, current_town.longitude + current_town.boundaries]]);
                        mymap.setView([current_town.latitude, current_town.longitude], 12);
                    } else {
                        console.log('Problem with the requested city. Еrror ' + request_city.status);
                    }
                };
                // REQUEST SECONDS
                request_locations = new XMLHttpRequest();

                request_locations.open('GET', base_url + 'api/places/get_by_city/second/lviv?format=json', true);
                request_locations.onload = function () {
                    locations = JSON.parse(this.response); // uncomment
                    if (request_locations.status >= 200 && request_locations.status < 400) {
                        // set locations to the map
                        console.log(locations);

                        lastGeoJSONlayer = addToMapGeoJSON();
                    } else {
                        console.log('Problem with the requested locations. Еrror ' + request_locations.status);
                    }
                };

                // remove zoom control on phones
                if (L.Browser.mobile) {
                    mymap.removeControl(mymap.zoomControl);
                }

                // MY GEO CONTROL
                GeoIconClass = L.Icon.extend({
                    options: {
                        iconSize: [12, 12], // size of the icon
                        iconAnchor: [6, 6] // point of the icon which will correspond to marker's location
                    }
                });
                GeoControl = L.Control.extend({
                    options: {
                        position: L.Browser.mobile ? 'topright' : 'topleft'
                        //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
                    },
                    onAdd: function onAdd(mymap) {
                        var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-geo');

                        container.innerHTML = "<img src=" + base_url + "static/img/gps32.png>";
                        if (L.Browser.mobile) {
                            container.style.width = "44px";
                            container.style.height = "44px";
                        } else {
                            var img = container.querySelector("img");
                            img.style.height = "18px";
                            img.style.width = "18px";
                            img.style.marginTop = "-9px";
                            img.style.marginLeft = "-9px";
                            console.log(img);
                        }
                        container.onclick = function () {
                            var popup_c = 1;
                            mymap.locate({setView: true, maxZoom: 14});

                            function onLocationFound(e) {
                                var radius = e.accuracy / 2;
                                // check if user in town
                                if (current_town.latitude - current_town.boundaries < e.latlng.lat && e.latlng.lat < current_town.latitude + current_town.boundaries && current_town.longitude - current_town.boundaries < e.latlng.lng && e.latlng.lng < current_town.longitude + current_town.boundaries) {

                                    // container.style.backgroundColor = '#008000';
                                    // TODO delete previuos geolocation

                                    if (last_m && last_c) {
                                        mymap.removeLayer(last_m);
                                        mymap.removeLayer(last_c);
                                        // console.log("HERE")
                                    }
                                    last_m = L.marker(e.latlng, {icon: new GeoIconClass({iconUrl: base_url + 'static/img/point.png'})}).addTo(mymap);
                                    last_c = L.circle(e.latlng, radius).addTo(mymap);
                                } else {
                                    if (popup_c) {
                                        alert("Здається ти за межами Львова. Скоро карта буде і у тебе)");
                                        popup_c = 0;
                                        mymap.setView(new L.LatLng(current_town.latitude, current_town.longitude), 12);
                                    }
                                }
                            }

                            function onLocationError(e) {
                                // e.message
                                if (popup_c) {
                                    alert(e.message);
                                    popup_c = 0;
                                }
                            }

                            mymap.on('locationerror', onLocationError);
                            mymap.on('locationfound', onLocationFound);
                        };
                        return container;
                    }
                });

                mymap.addControl(new L.Control.Fullscreen());
                mymap.on('fullscreenchange', function () {
                    if (mymap.isFullscreen()) {
                        mymap.gestureHandling.disable();
                    } else {
                        mymap.gestureHandling.enable();
                    }
                });

                mymap.addControl(new GeoControl());
                // END MY GEO CONTROL


                // mymap.scrollWheelZoom.disable();
                // mymap.on('focus', () => { mymap.scrollWheelZoom.enable(); });
                // mymap.on('blur', () => { mymap.scrollWheelZoom.disable(); });

                mymap.zoomControl = false;
                mymap.setMaxBounds([[current_town.latitude - current_town.boundaries, current_town.longitude - current_town.boundaries], [current_town.latitude + current_town.boundaries, current_town.longitude + current_town.boundaries]]);

                // set auto check to today and tomorrow
                day = new Date();
                weekday = new Array(7);

                weekday[0] = "sunday";
                weekday[1] = "monday";
                weekday[2] = "tuesday";
                weekday[3] = "wednesday";
                weekday[4] = "thursday";
                weekday[5] = "friday";
                weekday[6] = "saturday";

                today = weekday[day.getDay()];
                tomorrow = weekday[(day.getDay() + 1) % 7];
                show_everyday_second = false;

                activeDays.push(today, tomorrow);

                // get elements to auto check
                document.querySelector("." + today).click();
                document.querySelector("." + tomorrow).click();
                MarkerIconClass = L.Icon.extend({
                    options: {
                        iconSize: [17, 22], // size of the icon
                        iconAnchor: [8, 21], // point of the icon which will correspond to marker's location
                        popupAnchor: [1.5, -21] // point from which the popup should open relative to the iconAnchor
                    }
                });
                orangeIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_orange.png'});
                blueIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_blue.png'});
                darkblueIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_darkblue.png'});
                oliveIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_olive.png'});
                violetIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_violet.png'});
                yellowIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_yellow.png'});
                GreyIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_everyday.png'});
                redIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_red.png'});


                request_city.send();
                request_locations.send();

                // mymap.fitBounds(lastGeoJSONlayer)
                var buttonsdayslist = document.querySelectorAll(".button-day");
                for (var index = 0; index < buttonsdayslist.length; index++) {
                    var btn_day = buttonsdayslist[index];

                    btn_day.addEventListener('change', function (e) {
                        // delete previous layer
                        console.log(lastGeoJSONlayer);

                        if (lastGeoJSONlayer) lastGeoJSONlayer.remove();

                        if (this.classList[0] === "selectAllbutton") {
                            activeDays = ["sunday", "monday", 'tuesday', "wednesday", 'thursday', 'friday', 'saturday'];
                            if (show_everyday_second) activeDays.push("everyday");
                            var allButtons = document.querySelectorAll(".button-day input");
                            for (var i = 0; i < allButtons.length - 1; i++) {
                                var element = allButtons[i];
                                element.checked = true;
                            }
                            lastGeoJSONlayer = addToMapGeoJSON();
                            return;
                        }
                        if (this.classList[0] === "everyday") {
                            show_everyday_second = show_everyday_second ? false : true;
                        }
                        // if (this.classList[0] ===  "showeverydaysecond"){
                        //     activeDays.push("everyday")
                        //     show_everyday_second = true;
                        // }
                        console.log("after select all ");
                        if (this.querySelector('input').checked) {
                            console.log("push " + this.classList[0]);
                            activeDays.push(this.classList[0]);
                        } else {
                            var pos = activeDays.indexOf(this.classList[0]);
                            console.log("delete all " + this.classList[0] + "s now");
                            activeDays.splice(pos, 1);
                        }
                        console.log(activeDays);
                        lastGeoJSONlayer = addToMapGeoJSON();
                    });
                }
            })();
        }
    };

    function _classCallCheck$5(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var OMapStocks = function OMapStocks() {
        _classCallCheck$5(this, OMapStocks);

        this.name = 'o-map-stocks';
        console.log('%s module', this.name.toLowerCase());

        var mapfound = document.querySelector("#leafletmap-stocks");
        if (mapfound) {
            // ------------
            // Functions
            // ------------

            var createCustomPopup = function createCustomPopup(name, url, address, photo_url, description, sale) {
                // my custom pop-up
                // TODO set max characters to name

                if (sale) description = '<b style=\'color:red;\'>' + sale + '</b><br>' + description;
                var customPopUp = '<div class="pop-up"><div class="header"><span class="popupCustomCloser">Close</span><p class="title"><b>' + name + '</b></p><a href="' + url + '">View</a></div><hr><div class="content"><p><img class="image" src="' + photo_url + '">' + description + '</p></div><hr class="hr-last"><div class="pop-upfooter"><p>' + address + '.</p></div></div>';
                return customPopUp;
            };

            var onEachFeature = function onEachFeature(feature, layer) {
                layer.bindPopup(createCustomPopup(feature.properties.name, feature.properties.url, feature.properties.address, feature.properties.photo_url, feature.properties.description, feature.properties.sale));
                layer.on('popupopen', function (popup) {
                    if (document.querySelectorAll(".popupCustomCloser").length > 1) {
                        document.getElementsByClassName("popupCustomCloser")[1].addEventListener('click', function () {
                            document.getElementsByClassName('leaflet-popup-close-button')[0].click();
                        });
                    } else {
                        document.getElementsByClassName("popupCustomCloser")[0].addEventListener('click', function () {
                            document.getElementsByClassName('leaflet-popup-close-button')[0].click();
                        });
                    }
                });
            };

            // PSEUDO GLOBAL VARS


            // set auto check to today and tomorrow
            // var day=new Date();
            // var weekday=new Array(7);
            // weekday[0]="sunday";
            // weekday[1]="monday";
            // weekday[2]="tuesday";
            // weekday[3]="wednesday";
            // weekday[4]="thursday";
            // weekday[5]="friday";
            // weekday[6]="saturday";

            // var today = weekday[day.getDay()];
            // var tomorrow = weekday[((day.getDay() + 1) % 7)];
            // var show_everyday_second = false;
            // activeDays.push(today,tomorrow);

            // get elements to auto check
            // document.querySelector("."+today).click();
            // document.querySelector("."+tomorrow).click();


            // sort function
            var _addToMapGeoJSON = function _addToMapGeoJSON() {
                var geoJSONlayer = L.geoJSON(locations, {
                    pointToLayer: function pointToLayer(feature, latlng) {
                        if (feature.properties.sale.trim()) {
                            return L.marker(latlng, {icon: orangeIcon});
                        } else {
                            return L.marker(latlng, {icon: darkblueIcon});
                        }
                    },
                    // filter: activeDaysFilter,
                    onEachFeature: onEachFeature
                }).addTo(mymap);

                // mymap.fitBounds(geoJSONlayer.getBounds());
                return geoJSONlayer;
            };

            var lastGeoJSONlayer = undefined;
            // default town lviv
            var current_town = {
                "id": 1,
                "name": "Львів",
                "slug": "lviv",
                "longitude": 24.040532,
                "latitude": 49.837955,
                "boundaries": 0.2
            };

            var locations = [];

            // GEOJSON GET POINTS IN TOWN


            // ----------------------
            // INIT MAP
            // ----------------------
            // todo change to map mymap to map & leafletmap to map

            // var mymap = L.map('leafletmap').setView([current_town.latitude,current_town.longitude], 12);
            var mymap = L.map('leafletmap-stocks', {
                gestureHandling: true
            }).setView([current_town.latitude, current_town.longitude], 12);
            L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                // cause of this map box tile
                tileSize: 512,
                zoomOffset: -1,

                maxZoom: 18,
                minZoom: 11,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoiZmlmOTExIiwiYSI6ImNqMHZidmc1azAwMDcycXRiM2tkbnNkcWQifQ.X65DkBqIYJURurRhpT1UJA'
            }).addTo(mymap);

            // ----------------------
            // API GETTING NESSESARY INFO
            // ----------------------

            // request CITY
            var request_city = new XMLHttpRequest();
            request_city.open('GET', base_url + "api/places/get_cities/lviv?format=json", true);
            request_city.onload = function () {
                current_town = JSON.parse(this.response);
                if (request_city.status >= 200 && request_city.status < 400) {
                    // delete it
                    // var current_town = {
                    //     "id": 1,
                    //     "name": "Львів",
                    //     "slug": "lviv",
                    //     "longitude": 24.040532,
                    //     "latitude": 49.837955,
                    //     "boundaries": 0.2
                    //     // set view on requested city
                    //     // re set bounds
                    // };
                    mymap.setMaxBounds([[current_town.latitude - current_town.boundaries, current_town.longitude - current_town.boundaries], [current_town.latitude + current_town.boundaries, current_town.longitude + current_town.boundaries]]);
                    mymap.setView([current_town.latitude, current_town.longitude], 12);
                } else {
                    console.log('Problem with the requested city. Еrror ' + request_city.status);
                }
            };

            // REQUEST STOCKS
            var request_locations = new XMLHttpRequest();
            request_locations.open('GET', base_url + 'api/places/get_by_city/stock/lviv?format=json', true);
            request_locations.onload = function () {
                locations = JSON.parse(this.response);
                if (request_locations.status >= 200 && request_locations.status < 400) {
                    // set locations to the map
                    lastGeoJSONlayer = _addToMapGeoJSON();
                } else {
                    console.log('Problem with the requested locations. Еrror ' + request_locations.status);
                }
            };

            // remove zoom control on phones
            if (L.Browser.mobile) {
                mymap.removeControl(mymap.zoomControl);
            }

            // MY GEO CONTROL
            var GeoIconClass = L.Icon.extend({
                options: {
                    iconSize: [12, 12], // size of the icon
                    iconAnchor: [6, 6] // point of the icon which will correspond to marker's location
                }
            });

            var last_m, last_c;
            var GeoControl = L.Control.extend({
                options: {
                    position: L.Browser.mobile ? 'topright' : 'topleft'
                    //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
                },
                onAdd: function onAdd(mymap) {
                    var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-geo');

                    container.innerHTML = "<img src=" + base_url + "static/img/gps32.png>";
                    if (L.Browser.mobile) {
                        container.style.width = "44px";
                        container.style.height = "44px";
                    } else {
                        var img = container.querySelector("img");
                        img.style.height = "18px";
                        img.style.width = "18px";
                        img.style.marginTop = "-9px";
                        img.style.marginLeft = "-9px";
                        console.log(img);
                    }
                    container.onclick = function () {
                        var popup_c = 1;
                        mymap.locate({setView: true, maxZoom: 14});

                        function onLocationFound(e) {
                            var radius = e.accuracy / 2;
                            // check if user in town
                            if (current_town.latitude - current_town.boundaries < e.latlng.lat && e.latlng.lat < current_town.latitude + current_town.boundaries && current_town.longitude - current_town.boundaries < e.latlng.lng && e.latlng.lng < current_town.longitude + current_town.boundaries) {

                                // container.style.backgroundColor = '#008000';
                                // TODO delete previuos geolocation

                                if (last_m && last_c) {
                                    mymap.removeLayer(last_m);
                                    mymap.removeLayer(last_c);
                                    // console.log("HERE")
                                }
                                last_m = L.marker(e.latlng, {icon: new GeoIconClass({iconUrl: base_url + 'static/img/point.png'})}).addTo(mymap);
                                last_c = L.circle(e.latlng, radius).addTo(mymap);
                            } else {
                                if (popup_c) {
                                    alert("Здається ти за межами Львова. Скоро карта буде і у тебе)");
                                    popup_c = 0;
                                    mymap.setView(new L.LatLng(current_town.latitude, current_town.longitude), 12);
                                }
                            }
                        }

                        function onLocationError(e) {
                            // e.message
                            if (popup_c) {
                                alert(e.message);
                                popup_c = 0;
                            }
                        }

                        mymap.on('locationerror', onLocationError);
                        mymap.on('locationfound', onLocationFound);
                    };
                    return container;
                }
            });

            mymap.addControl(new L.Control.Fullscreen());
            mymap.on('fullscreenchange', function () {
                if (mymap.isFullscreen()) {
                    mymap.gestureHandling.disable();
                } else {
                    mymap.gestureHandling.enable();
                }
            });
            mymap.addControl(new GeoControl());
            // END MY GEO CONTROL


            // mymap.scrollWheelZoom.disable();
            // mymap.on('focus', () => { mymap.scrollWheelZoom.enable(); });
            // mymap.on('blur', () => { mymap.scrollWheelZoom.disable(); });

            mymap.zoomControl = false;
            mymap.setMaxBounds([[current_town.latitude - current_town.boundaries, current_town.longitude - current_town.boundaries], [current_town.latitude + current_town.boundaries, current_town.longitude + current_town.boundaries]]);
            var MarkerIconClass = L.Icon.extend({
                options: {
                    iconSize: [17, 22], // size of the icon
                    iconAnchor: [8, 21], // point of the icon which will correspond to marker's location
                    popupAnchor: [1.5, -21] // point from which the popup should open relative to the iconAnchor
                }
            });

            var orangeIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_orange.png'}),
                darkblueIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_darkblue.png'});

            request_city.send();
            request_locations.send();

            // lastGeoJSONlayer = addToMapGeoJSON();

        }
    };

    function _classCallCheck$6(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var OShopItemNastya = function OShopItemNastya() {
        _classCallCheck$6(this, OShopItemNastya);

        this.name = 'o-shop-item-nastya';
        console.log('%s module', this.name.toLowerCase());
        // start
        var Check_OShopItemNastya = document.querySelector('.o-shop-item-nastya');
        if (Check_OShopItemNastya) {
            console.log("NASTYA HELLO");

            var setHeight = function setHeight(div) {
                var blocks = document.querySelectorAll(div);
                for (var i = 0, length = blocks.length; i < length; i++) {
                    var item = blocks[i];
                    item.style.height = item.offsetWidth + 'px';
                }
            };
            // Їх не може бути 1 або 3 бо вони повторюються ще в глайд слайдс
            // Хіба .length ділити на два тоді буде працювати
            if (document.querySelectorAll('.item-img').length / 2 == 1) {
                document.querySelector('.item-information').classList.add('single');
            } else if (document.querySelectorAll('.item-img').length / 2 == 3) {
                document.querySelector('.item-information').classList.add('three');
            }

            var slides = document.querySelectorAll('.glide');

            var p = new Promise(function (resolve, reject) {
                for (var i = 0; i < slides.length; i++) {
                    var slideChildren = slides[i].querySelector('.glide__slides').children;

                    // Create bullets for each slide..
                    for (var j = 0; j < slideChildren.length; j++) {
                        var button = document.createElement('button');
                        button.classList.add('glide__bullet');
                        button.setAttribute('data-glide-dir', '=' + j);

                        slides[i].querySelector('.glide__bullets').appendChild(button);
                    }
                }
            });

            if (window.innerWidth < 992) {
                (function () {
                    //slider
                    baguetteBox.run('.glide__slides', {
                        noScrollbars: true,
                        captions: false
                    });

                    p.then(new Glide('.glide', {
                        // type: 'carousel',
                        type: 'slider',
                        rewind: false,
                        breakpoints: {
                            992: {
                                perView: 2,
                                focusAt: 0
                            },
                            660: {
                                perView: 2
                            },
                            476: {
                                perView: 1
                            }
                        }
                    }).mount()).catch(function (reason) {
                        console.log('Handle rejected promise (' + reason + ') here.');
                    });

                    var dropdownContent = document.querySelectorAll('.content');
                    var handleDropdown = document.querySelectorAll('.handle-dropdown');

                    var _loop = function _loop(i, length) {
                        var maxHeight = dropdownContent[i].clientHeight;
                        dropdownContent[i].style.maxHeight = '0px';

                        var active = false;

                        handleDropdown[i].addEventListener('click', function (e) {
                            if (!active) {
                                this.classList.add('open');
                                dropdownContent[i].classList.add('show');
                                dropdownContent[i].style.maxHeight = maxHeight + 'px';
                                active = true;
                            } else {
                                this.classList.remove('open');
                                dropdownContent[i].classList.remove('show');
                                dropdownContent[i].style.maxHeight = '0px';
                                active = false;
                            }
                        });
                    };

                    for (var i = 0, length = dropdownContent.length; i < length; i++) {
                        _loop(i, length);
                    }
                })();
            } else {
                baguetteBox.run('.gallery', {
                    ignoreClass: 'ignore-on-desc'
                });
            }

            setHeight('.item-img');

            if (window.innerWidth >= 992) {
                var descriptionBlock = document.querySelector('.item-price-wrapper');
                var containerBlock = descriptionBlock.parentElement;
                var headerHeight = document.querySelector('.o-header').offsetHeight;
                var leftPosition = descriptionBlock.getBoundingClientRect().left;
                var scrollPos = 0;
                // window.onscroll = () => {
                window.onscroll = function () {
                    // addScrollClass() or MY SCROLL HANDLER

                    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
                    var burger_menu = document.querySelector('.o-header__navbar');
                    if (burger_menu) {
                        if (scrolled > 90) {
                            // burger_menu.style.backgroundColor = '#00132B';
                            burger_menu.classList.add("scrolled");
                        } else {
                            // burger_menu.style.backgroundColor = 'transparent';
                            burger_menu.classList.remove("scrolled");
                        }
                    }
                    // END MY SCROLL HANDLER

                    // was 0. TODO FIX IT headerHeight + distanse to containerBlock
                    // if (containerBlock.getBoundingClientRect().top <= 0) {
                    if (containerBlock.getBoundingClientRect().top <= 100) {
                        descriptionBlock.classList.add('fixed');
                        descriptionBlock.style.left = leftPosition + 'px';

                        if (Math.abs(containerBlock.getBoundingClientRect().top - headerHeight - 30) > containerBlock.getBoundingClientRect().height - descriptionBlock.offsetHeight) {
                            descriptionBlock.classList.remove('fixed');
                            descriptionBlock.classList.add('fixed-bottom');
                            descriptionBlock.style.left = 'initial';
                            // check scroll up
                            if (document.body.getBoundingClientRect().top > scrollPos && descriptionBlock.getBoundingClientRect().top - headerHeight - 20 > 0) {
                                // headerHeight - 20 = 100 and it's top parameter choosed in .fixed class
                                descriptionBlock.classList.remove('fixed-bottom');
                                descriptionBlock.classList.add('fixed');
                                descriptionBlock.style.left = leftPosition + 'px';
                            }
                        }
                    } else {
                        descriptionBlock.classList.remove('fixed');
                    }
                    // Set current scroll position.
                    scrollPos = document.body.getBoundingClientRect().top;
                };
            }

            // finish
        }
    };

    function _classCallCheck$7(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var OMapSingeIcon = function OMapSingeIcon() {
        _classCallCheck$7(this, OMapSingeIcon);

        this.name = 'o-map-singe-icon';
        console.log('%s module', this.name.toLowerCase());
        var mapfound = document.querySelector("#leafletmap-single");
        if (mapfound) {

            // REQUEST SINGLE PLACE
            // get/<model>/<id>
            var request_location = new XMLHttpRequest();
            // request_location.open('GET', 'https://jsonplaceholder.typicode.com/users', true);
            request_location.open('GET', base_url + "api/places/get/"+ api_request_postfix+"?format=json", true);
            console.log( base_url + "api/places/get/"+ api_request_postfix+"?format=json");

            request_location.onload = function () {
                var location = JSON.parse(this.response);
                if (request_location.status >= 200 && request_location.status < 400) {
                    // delete it
                    console.log(location)
                    // var location = {
                    //     "type": "Feature",
                    //     "properties": {
                    //         "name": "общага",
                    //         "day": "monday",
                    //         "url": "/iteminshop",
                    //         "address": "Сахарова 25",
                    //         "photo_url": "../img/second_item.jpg",
                    //         "description": "the best shop da world epta"
                    //     },
                    //     "geometry": {
                    //         "type": "Point",
                    //         "coordinates": [24.012637, 49.825941]
                    //     }
                    // };
                    console.log(location);
                    var coordinates = location.geometry.coordinates;
                    mymap.setView([coordinates[1], coordinates[0]], 12);
                    L.geoJSON(location, {
                        pointToLayer: function pointToLayer(feature, latlng) {
                            return L.marker(latlng, {icon: orangeIcon});
                        }
                    }).addTo(mymap);
                } else {
                    console.log('Problem with the requested city. Еrror ' + request_city.status);
                }
            };
            // set view to city which is needed
            var mymap = L.map('leafletmap-single', {
                gestureHandling: true
            }).setView([49.837955, 24.040532], 12);
            L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                // cause of this map box tile
                tileSize: 512,
                zoomOffset: -1,
                maxZoom: 18,
                minZoom: 11,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoiZmlmOTExIiwiYSI6ImNqMHZidmc1azAwMDcycXRiM2tkbnNkcWQifQ.X65DkBqIYJURurRhpT1UJA'
            }).addTo(mymap);

            // MY GEO CONTROL
            var GeoIconClass = L.Icon.extend({
                options: {
                    iconSize: [12, 12], // size of the icon
                    iconAnchor: [6, 6] // point of the icon which will correspond to marker's location
                }
            });

            var last_m, last_c;
            var GeoControl = L.Control.extend({
                options: {
                    position: L.Browser.mobile ? 'topright' : 'topleft'
                    //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
                },
                onAdd: function onAdd(mymap) {
                    var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-geo');

                    container.style.backgroundColor = 'white';
                    container.innerHTML = "<img src=" + base_url + "static/img/gps32.png>";
                    if (L.Browser.mobile) {
                        container.style.width = "44px";
                        container.style.height = "44px";
                    } else {
                        var img = container.querySelector("img");
                        img.style.height = "18px";
                        img.style.width = "18px";
                        img.style.marginTop = "-9px";
                        img.style.marginLeft = "-9px";
                        console.log(img);
                    }
                    container.onclick = function () {
                        var popup_c = 1;
                        mymap.locate({setView: true, maxZoom: 14});

                        function onLocationFound(e) {
                            var radius = e.accuracy / 2;

                            // container.style.backgroundColor = '#008000';
                            // TODO delete previuos geolocation

                            if (last_m && last_c) {
                                mymap.removeLayer(last_m);
                                mymap.removeLayer(last_c);
                                // console.log("HERE")
                            }
                            last_m = L.marker(e.latlng, {icon: new GeoIconClass({iconUrl: base_url + 'static/img/point.png'})}).addTo(mymap);
                            last_c = L.circle(e.latlng, radius).addTo(mymap);
                        }

                        function onLocationError(e) {
                            // e.message
                            if (popup_c) {
                                alert(e.message);
                                popup_c = 0;
                            }
                        }

                        mymap.on('locationerror', onLocationError);
                        mymap.on('locationfound', onLocationFound);
                    };
                    return container;
                }
            });
            if (L.Browser.mobile) {
                mymap.removeControl(mymap.zoomControl);
            }
            mymap.addControl(new L.Control.Fullscreen());
            mymap.on('fullscreenchange', function () {
                if (mymap.isFullscreen()) {
                    mymap.gestureHandling.disable();
                } else {
                    mymap.gestureHandling.enable();
                }
            });
            mymap.addControl(new GeoControl());
            // END MY GEO CONTROL

            // Defining an icon class
            var MarkerIconClass = L.Icon.extend({
                options: {
                    iconSize: [17, 22], // size of the icon
                    iconAnchor: [8, 21], // point of the icon which will correspond to marker's location
                    popupAnchor: [1.5, -21] // point from which the popup should open relative to the iconAnchor
                }
            });

            var orangeIcon = new MarkerIconClass({iconUrl: base_url + 'static/img/marker_orange.png'});

            request_location.send();
        }
    };

    function _classCallCheck$8(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var OAllplaces = function OAllplaces() {
        _classCallCheck$8(this, OAllplaces);

        this.name = 'o-allplaces';
        console.log('%s module', this.name.toLowerCase());
    };

    function _classCallCheck$9(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var MPlaceDescription = function MPlaceDescription() {
        _classCallCheck$9(this, MPlaceDescription);

        this.name = 'm-place-description';
        console.log('%s module', this.name.toLowerCase());
        var m_place_desc = document.querySelector(".m-place-description");
        if (m_place_desc && m_place_desc.querySelector(".info-holder--more_info")) {
            var second_icon_holder = m_place_desc.querySelectorAll(".icon-holder")[1];

            second_icon_holder.addEventListener('click', function () {
                m_place_desc.querySelector(".info-holder--more_info").classList.toggle("active");
                // console.logs(second_icon_holder)
                second_icon_holder.querySelector(".icon").classList.toggle("expanded");
            });
        }
    };

    function _classCallCheck$10(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var MPlaceGallery = function MPlaceGallery() {
        _classCallCheck$10(this, MPlaceGallery);

        this.name = 'm-place-gallery';
        console.log('%s module', this.name.toLowerCase());
        // var height_of_desc

        var glide_found = document.querySelector("#glideslider");
        if (glide_found) {
            var glide = new Glide('#glideslider', {
                type: 'slider',
                rewind: false,
                perView: 2,
                focusAt: 'center',
                breakpoints: {
                    700: {
                        perView: 1
                    },
                    480: {
                        perView: 1
                    }
                }
            });

            glide.mount();
            //TODO glide.mount({ Bullets })
            // or like in old design
        }
    };

// =========================================
// Initialization
// =========================================
    console.table(Config);
    new Footer();
    new PageSwitcher();
    new OMap();
    new OMapStocks();
    new OMapSingeIcon();
    new DropDownSelect();
    new MShopTopbar();
    new OAllplaces();
    new MPlaceDescription();
    new MPlaceGallery();
    new OShopItemNastya();

}());
